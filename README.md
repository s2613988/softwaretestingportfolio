# Project Readme

## Overview

This project is developed in Java 17, with corresponding JavaFX files provided in the "lib" folder. To run the program from the .jar file, it is crucial to link the JavaFX module.

## Execution Command

Use the following command to run the program:

```bash
java --module-path ./lib/javafx-sdk-17.0.10/lib --add-modules javafx.controls,javafx.fxml -jar JaVelo-master.jar
