package ch.epfl.javelo.routing;

/**
 * A cost-function evaluate the cost of an edge.
 * It is necessary for the shortest path algorithm.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public interface CostFunction {

    /**
     * Gives the multiplicative factor for an edge starting from a node (nodeId).
     *
     * @param nodeId ID of the node
     * @param edgeId ID of the edge. Not the index in the list of edges of the node
     * @return The weight factor of a given edge. Must be >= 1
     */
    double costFactor(int nodeId, int edgeId);
}
