package ch.epfl.javelo.routing;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.Preconditions;
import ch.epfl.javelo.projection.PointCh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Immutable class representing a simple itinerary that contains edges.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class SingleRoute implements Route {

    private final List<Edge> edges;
    private final double[] lengthList;
    private List<PointCh> points;

    /**
     * Constructor of a SingleRoute.
     *
     * @param edges List of edges.
     */
    public SingleRoute(List<Edge> edges) {
        Preconditions.checkArgument(edges.size() > 0);

        double lengthAddedUp = 0.0;
        lengthList = new double[edges.size() + 1];
        lengthList[0] = lengthAddedUp;

        for (int i = 0; i < edges.size(); i++) {
            lengthAddedUp += edges.get(i).length();
            lengthList[i + 1] = lengthAddedUp;
        }
        this.edges = List.copyOf(edges);
        setPoints();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOfSegmentAt(double position) {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double length() {
        return lengthList[lengthList.length - 1];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Edge> edges() {
        return edges;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PointCh> points() {
        return List.copyOf(points);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointCh pointAt(double position) {
        int index = binaryToRealIndex(position);
        position = Math2.clamp(0, position, length());

        return edges.get(index).pointAt(position - lengthList[index]);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double elevationAt(double position) {
        int index = binaryToRealIndex(position);
        position = Math2.clamp(0, position, length());

        if (edges.get(index).profile() == null) {
            return Double.NaN;
        }

        return edges.get(index).elevationAt(position - lengthList[index]);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int nodeClosestTo(double position) {
        int index = binaryToRealIndex(position);
        double positionInEdge = position - lengthList[index];

        if (edges.get(index).length() / 2 >= positionInEdge) {
            return edges.get(index).fromNodeId();
        } else {
            return edges.get(index).toNodeId();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoutePoint pointClosestTo(PointCh point) {
        RoutePoint closeOne = RoutePoint.NONE;
        int counter = 0;

        for (Edge edge : edges) {
            double positionOnEdge = Math2.clamp(0, edge.positionClosestTo(point), edge.length());
            closeOne = closeOne.min(edge.pointAt(positionOnEdge), positionOnEdge + lengthList[counter],
                    point.distanceTo(edge.pointAt(positionOnEdge)));
            counter++;
        }
        return closeOne;
    }

    /**
     * Private method that converts the index given by the binary search to the real index of the edge
     * based on the position on the itinerary.
     * The real index is used in pointAt, elevationAt and nodeClosestTo.
     *
     * @param position the position in the SingleRoute
     * @return the real index of the edge
     */
    private int binaryToRealIndex(double position) {
        position = Math2.clamp(0, position, length());
        int indexBinary = Arrays.binarySearch(lengthList, position);
        int realIndex;

        if (position == length()) {
            realIndex = indexBinary - 1;
        } else if (indexBinary >= 0) {
            realIndex = indexBinary;
        } else {
            realIndex = Math.abs(indexBinary) - 2;
        }

        return realIndex;
    }

    /**
     * Private method that sets the points of the route, directly called in the constructor.
     */
    private void setPoints(){
        List<PointCh> points = new ArrayList<>();
        points.add(edges.get(0).fromPoint());

        for (Edge e : edges) {
            points.add(e.toPoint());
        }
        this.points = points;
    }
}
