package ch.epfl.javelo.routing;

import ch.epfl.javelo.Functions;
import ch.epfl.javelo.Preconditions;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.function.DoubleUnaryOperator;

/**
 * Immutable class that gives some properties of the profile of an itinerary.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class ElevationProfile {

    private final double length;
    private final float[] elevationSamples;
    private double minElevation;
    private double maxElevation;
    private double totalAscent;
    private double totalDescent;

    /**
     * Constructor of an ElevationProfile.
     *
     * @param length           the length of the profile
     * @param elevationSamples the samples of the profile
     * @throws IllegalArgumentException if the length is equal or lower to 0
     *                                  and if the number of samples is lower than 2
     */
    public ElevationProfile(double length, float[] elevationSamples) {
        Preconditions.checkArgument(length > 0 && elevationSamples.length >= 2);
        this.length = length;
        this.elevationSamples = Arrays.copyOf(elevationSamples, elevationSamples.length);
        setTotals();
        setMaxAndMin();
    }

    /**
     * Computes the length of the profile.
     *
     * @return the length of the profile
     */
    public double length() {
        return length;
    }

    /**
     * Computes the minimal altitude of the profile.
     *
     * @return the minimal altitude of the profile
     */
    public double minElevation() {
        return minElevation;
    }

    /**
     * Computes the maximal altitude (in meters) of the profile.
     *
     * @return the maximal altitude (in meters) of the profile
     */
    public double maxElevation() {
        return maxElevation;
    }

    /**
     * Computes the total positive elevation of the profiles (in meters).
     *
     * @return the total positive elevation of the profiles
     */
    public double totalAscent() {
        return totalAscent;
    }

    /**
     * Computes the total negative elevation of the profiles (in meters).
     *
     * @return the total positive elevation of the profiles
     */
    public double totalDescent() {
        return totalDescent;
    }

    /**
     * Computes the altitude at a given position (can be bigger or lower than the min and maximal value).
     *
     * @param position the position on the profile
     * @return the altitude at a given position (can be bigger or lower than the min and maximal value)
     */
    public double elevationAt(double position) {
        DoubleUnaryOperator elevate = Functions.sampled(elevationSamples, length);
        return elevate.applyAsDouble(position);
    }

    /**
     * Sets the values of the attributes totalAscent and totalDescent in order to avoid unnecessary
     * recalculation.
     */
    private void setTotals(){
        double totaldescent = 0;
        double totalascent = 0;

        for (int i = 0; i < elevationSamples.length - 1; i++) {
            double difference = elevationSamples[i + 1] - elevationSamples[i];
            if (difference < 0) {
                totaldescent -= difference;
            } else
                totalascent += difference;
        }
        this.totalDescent = totaldescent;
        this.totalAscent = totalascent;
    }
    /**
     * Sets the values of the attributes minElevation and maxElevation in order to avoid unnecessary
     * recalculation.
     */
    private void setMaxAndMin(){
        DoubleSummaryStatistics stats = new DoubleSummaryStatistics();
        for (float elevationSample : elevationSamples) {
            stats.accept(elevationSample);
        }
        this.minElevation = stats.getMin();
        this.maxElevation = stats.getMax();
    }
}
