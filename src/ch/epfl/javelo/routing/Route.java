package ch.epfl.javelo.routing;

import ch.epfl.javelo.projection.PointCh;

import java.util.List;

/**
 * Interface representing an itinerary.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public interface Route {
    /**
     * Computes the index of the segment at a given position.
     *
     * @param position the position
     * @return the index of the segment at a given position
     */
    int indexOfSegmentAt(double position);

    /**
     * Computes the length of the itinerary.
     *
     * @return the length of the itinerary (in meters)
     */
    double length();

    /**
     * Gives the total list of edges in an itinerary.
     *
     * @return the total list of edges in an itinerary
     */
    List<Edge> edges();

    /**
     * Gives the total list of points in an itinerary.
     *
     * @return the total list of points in an itinerary
     */
    List<PointCh> points();

    /**
     * Computes the point at a given position on an itinerary.
     *
     * @param position the position on the itinerary
     * @return the point at a given position on an itinerary
     */
    PointCh pointAt(double position);

    /**
     * Computes the altitude at a given position on an itinerary.
     *
     * @param position the position on the itinerary
     * @return the altitude at a given position on an itinerary
     */
    double elevationAt(double position);

    /**
     * Computes the identity of the closest node belonging
     * to an itinerary at a given position.
     *
     * @param position the position on the itinerary
     * @return the identity of the closest node
     * belonging to an itinerary at a given position
     */
    int nodeClosestTo(double position);

    /**
     * Gives the point (RoutePoint) of an itinerary that
     * is the closest to a given point (PointCh).
     *
     * @param point the given point
     * @return the point of an itinerary that
     * is the closest to a given point (PointCh)
     */
    RoutePoint pointClosestTo(PointCh point);
}
