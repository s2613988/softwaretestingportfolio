package ch.epfl.javelo.routing;

import ch.epfl.javelo.projection.PointCh;

/**
 * Class representing the point of an itinerary that is the closest to
 * a referenced point.
 *
 * @param point               a given point as a reference
 * @param position            the position of the closest point on a Route
 * @param distanceToReference the distance between the closest
 *                            point on a Route and the reference
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public record RoutePoint(PointCh point, double position, double distanceToReference) {
    /**
     * NONE, represents an non-existent point.
     */
    public static final RoutePoint NONE = new RoutePoint(null, Double.NaN, Double.POSITIVE_INFINITY);

    /**
     * Computes a point exactly the same as @this except with a
     * position that is shifted by positionDifference (double).
     *
     * @param positionDifference the value of the shifted position.
     * @return a point exactly the same as @this except with a
     * position that is shifted by positionDifference (double)
     */
    public RoutePoint withPositionShiftedBy(double positionDifference) {
        return positionDifference == 0 ? this:
                new RoutePoint(this.point, position + positionDifference, distanceToReference);
    }

    /**
     * Gives @this if its distance to reference (distanceToReference)
     * is equal or lower to that (RoutePoint) distance to reference,
     * else it returns that (RoutePoint).
     *
     * @param that the compared routePoint
     * @return @this (RoutePoint) if its distance to reference
     * (distanceToReference) is equal or lower to that (RoutePoint),
     * else it returns that (RoutePoint)
     */
    public RoutePoint min(RoutePoint that) {
        return (distanceToReference <= that.distanceToReference()) ? this : that;
    }

    /**
     * Gives @this if its distance to reference (distanceToReference) is
     * equal or lower to that (RoutePoint) distance to reference, else it
     * returns a new RoutePoint which has as an input the ones given in min.
     *
     * @param thatPoint               the point
     * @param thatPosition            the position
     * @param thatDistanceToReference the distance to reference
     * @return @this if its distance to reference (distanceToReference) is
     * equal or lower to that (RoutePoint) distance to reference,
     * else it returns a new RoutePoint which has as an input the ones given in min
     */
    public RoutePoint min(PointCh thatPoint, double thatPosition, double thatDistanceToReference) {
        return (this.distanceToReference <= thatDistanceToReference) ? this :
                new RoutePoint(thatPoint, thatPosition, thatDistanceToReference);
    }
}
