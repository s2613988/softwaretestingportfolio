package ch.epfl.javelo.routing;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.Preconditions;

import java.util.Arrays;

/**
 * Can compute the profile of a route
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class ElevationProfileComputer {

    /**
     * Private constructor to avoid instantiation.
     */
    private ElevationProfileComputer() {
    }

    /**
     * Computes the profile of a route.
     *
     * @param route         Route
     * @param maxStepLength Maximum space between the samples
     * @return Profile of the route
     * @throws IllegalArgumentException maxStepLength not strictly positive
     */
    public static ElevationProfile elevationProfile(Route route, double maxStepLength) {
        Preconditions.checkArgument(maxStepLength > 0);

        int n_samples = ((int) Math.ceil(route.length() / maxStepLength)) + 1;
        double step = route.length() / (double) (n_samples - 1);
        float[] profileSamples = new float[n_samples];

        for (int i = 0; i < n_samples; i++) {
            profileSamples[i] = (float) route.elevationAt(i * step);
        }

        //Step 1
        for (int i = 0; i < n_samples; i++) {
            if (!Float.isNaN(profileSamples[i])) {
                if (i != 0) {
                    Arrays.fill(profileSamples, 0, i, profileSamples[i]);
                }
                break;
            }
            if (i == n_samples - 1) { //Apply to profile with all NaN values
                Arrays.fill(profileSamples, 0, n_samples, 0);
            }
        }

        //Step 2
        for (int i = n_samples - 1; i >= 0; i--) {
            if (!Float.isNaN(profileSamples[i])) {
                if (i != n_samples - 1) {
                    Arrays.fill(profileSamples, i + 1, n_samples, profileSamples[i]);
                }
                break;
            }
        }

        //Step 3
        int firstIndex = 0;
        int lastIndex;
        boolean UndefinedArea = false;
        for (int i = 0; i < n_samples; i++) {

            if (!UndefinedArea && Float.isNaN(profileSamples[i])) {
                firstIndex = i - 1;
                UndefinedArea = true;
            }
            if (UndefinedArea && !Float.isNaN(profileSamples[i])) {
                lastIndex = i;
                for (int j = firstIndex + 1; j < lastIndex; j++) {
                    profileSamples[j] = (float) Math2.interpolate(
                            profileSamples[firstIndex], profileSamples[lastIndex], (j - firstIndex) * (1f / (lastIndex - firstIndex)));
                }
                UndefinedArea = false;
            }
        }
        return new ElevationProfile(route.length(), profileSamples);
    }
}
