package ch.epfl.javelo.routing;

import ch.epfl.javelo.Preconditions;
import ch.epfl.javelo.data.Graph;


import java.util.*;

/**
 * Computes the shortest route between two nodes on a given graph.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public class RouteComputer {

    private final Graph graph;
    private final CostFunction costFunction;

    private static final float INITIAL_DISTANCE = Float.POSITIVE_INFINITY;
    private static final float NODE_ALREADY_EXPLORED_VALUE = Float.NEGATIVE_INFINITY;

    /**
     * Builds a route planner for a graph and a cost-function.
     *
     * @param graph        Graph used to compute the routes
     * @param costFunction Function used to evaluate the cost of every edges
     */
    public RouteComputer(Graph graph, CostFunction costFunction) {
        this.graph = graph;
        this.costFunction = costFunction;
    }

    /**
     * Comparable used to sort the nodes in the exploration PriorityQueue.
     * @param nodeId ID of the node
     * @param distance Shortest distance to reach this node
     */
    record WeightedNode(int nodeId, float distance)
            implements Comparable<WeightedNode> {

        @Override
        public int compareTo(WeightedNode that) {
            return Float.compare(this.distance, that.distance);
        }
    }

    /**
     * Computes the minimal cost route between two nodes.
     *
     * @param startNodeId ID of the first node
     * @param endNodeId   ID of the last node
     * @return The shortest route or null if no route is found
     * @throws IllegalArgumentException startNodeId is equal to endNodeId
     */
    public Route bestRouteBetween(int startNodeId, int endNodeId) {
        Preconditions.checkArgument(startNodeId != endNodeId);

        Float[] distance = new Float[graph.nodeCount()];
        int[] predecessor = new int[graph.nodeCount()];
        PriorityQueue<WeightedNode> enExploration = new PriorityQueue<>();

        Arrays.fill(distance, INITIAL_DISTANCE);

        distance[startNodeId] = 0f;
        enExploration.add(new WeightedNode(startNodeId, distance[startNodeId]));

        int node;
        int targetNode;
        float d;
        while (!enExploration.isEmpty()) {

            node = enExploration.poll().nodeId;

            //creation of the route if we found the endNode
            if (node == endNodeId) {
                return createRoute(predecessor,startNodeId,endNodeId);
            }

            //Further exploration
            if (distance[node] != NODE_ALREADY_EXPLORED_VALUE) {
                for (int i = 0; i < graph.nodeOutDegree(node); i++) {
                    targetNode = graph.edgeTargetNodeId(graph.nodeOutEdgeId(node, i));
                    if (distance[targetNode] != NODE_ALREADY_EXPLORED_VALUE){ //optimization avoid adding node to exploration that have already been explored
                        d = distance[node] + (float) (costFunction.costFactor(node, graph.nodeOutEdgeId(node, i)) * graph.edgeLength(graph.nodeOutEdgeId(node, i)));
                        if (d < distance[targetNode]) {
                            distance[targetNode] = d;
                            predecessor[targetNode] = node;
                            enExploration.add(new WeightedNode(targetNode, d + (float) graph.nodePoint(node).distanceTo(graph.nodePoint(targetNode))));
                        }
                    }
                }
                distance[node] = NODE_ALREADY_EXPLORED_VALUE;
            }
        }
        return null;
    }

    /**
     * Builds the route found by the shortest path algorithm.
     * @param predecessor Solution found by the shortest path algorithm
     * @param startNodeId First Node of the route
     * @param endNodeId Last node of the route
     * @return The route
     */
    private Route createRoute(int[] predecessor,int startNodeId, int endNodeId){
        int node = endNodeId;
        LinkedList<Edge> edges = new LinkedList<>();
        while (node != startNodeId) {
            for (int i = 0; i < graph.nodeOutDegree(predecessor[node]); i++) {
                if (graph.edgeTargetNodeId(graph.nodeOutEdgeId(predecessor[node], i)) == node) {
                    Edge edge = Edge.of(graph, graph.nodeOutEdgeId(predecessor[node], i), predecessor[node], node);
                    if(edge.length() != 0){
                        edges.addFirst(edge);
                    }
                    node = predecessor[node];
                    break;
                }
            }
        }
        return new SingleRoute(edges);
    }
}
