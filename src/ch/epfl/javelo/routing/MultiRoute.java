package ch.epfl.javelo.routing;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.Preconditions;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents multiple itineraries and is composed of multiple segments
 * (sequences of itineraries : SingleRoute or MultiRoute).
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class MultiRoute implements Route {
    private final List<Route> segments;

    /**
     * MultiRoute constructor.
     *
     * @param segments a list of routes
     *                 (can be composed of a MultiRoute or a SingleRoute)
     * @throws IllegalArgumentException: if the List of routes is empty
     */
    public MultiRoute(List<Route> segments) {
        Preconditions.checkArgument(!segments.isEmpty());
        this.segments = List.copyOf(segments);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOfSegmentAt(double position) {
        int index = 0;
        double totalLength = 0.0;
        position = Math2.clamp(0, position, length());

        for (Route current : segments) {
            totalLength += current.length();
            if (totalLength >= position) {
                index += current.indexOfSegmentAt(position - (totalLength - current.length()));
                break;
            } else {
                index += current.indexOfSegmentAt(current.length()) + 1;
            }
        }
        return index;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double length() {
        double length = 0;

        for (Route route : segments) {
            length += route.length();
        }
        return length;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Edge> edges() {
        List<Edge> edges = new ArrayList<>();

        for (Route route : segments) {
            edges.addAll(route.edges());
        }
        return edges;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PointCh> points() {
        int id = 0;
        List<PointCh> pointsWithoutDuplicates = new ArrayList<>();
        pointsWithoutDuplicates.add(pointAt(0));

        for (Route route : segments) {
            pointsWithoutDuplicates.remove(id);
            pointsWithoutDuplicates.addAll(route.points());

            id +=  route.points().size() - 1;
        }
        return pointsWithoutDuplicates;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointCh pointAt(double position) {
        double totalLength = 0.0;
        PointCh point = new PointCh(SwissBounds.MIN_E, SwissBounds.MIN_N);
        position = Math2.clamp(0, position, length());

        for (Route current : segments) {
            totalLength += current.length();
            if (totalLength >= position) {
                point = current.pointAt(position - (totalLength - current.length()));
                break;
            }
        }
        return point;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double elevationAt(double position) {
        double totalLength = 0.0;
        double elevation = 0.0;
        position = Math2.clamp(0, position, length());

        for (Route current : segments) {
            totalLength += current.length();
            if (totalLength >= position) {
                elevation = current.elevationAt(position - (totalLength - current.length()));
                break;
            }
        }
        return elevation;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int nodeClosestTo(double position) {
        double totalLength = 0.0;
        int nodeID = 0;
        position = Math2.clamp(0, position, length());

        for (Route current : segments) {
            totalLength += current.length();
            if (totalLength >= position) {
                nodeID = current.nodeClosestTo(position - (totalLength - current.length()));
                break;
            }
        }
        return nodeID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoutePoint pointClosestTo(PointCh point) {
        double position = 0.0;
        RoutePoint routePointMinimal = RoutePoint.NONE;
        RoutePoint newRoutePoint;

        for (Route route : segments) {
            newRoutePoint = routePointMinimal.min(route.pointClosestTo(point));
            if (newRoutePoint != routePointMinimal) {
                newRoutePoint = newRoutePoint.withPositionShiftedBy(position);
                routePointMinimal = newRoutePoint;
            }
            position += route.length();
        }

        return routePointMinimal;
    }
}
