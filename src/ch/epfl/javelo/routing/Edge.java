package ch.epfl.javelo.routing;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.PointCh;

import java.util.function.DoubleUnaryOperator;

/**
 * Class that represents an Edge.
 *
 * @param fromNodeId the ID of the Node from which the edge is coming from
 * @param toNodeId   the ID of the Node to which the edge is going to
 * @param fromPoint  the point from which the edge is coming from
 * @param toPoint    the point to which the edge is going to
 * @param length     the length of the edge
 * @param profile    the profile of the edge
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public record Edge(int fromNodeId, int toNodeId, PointCh fromPoint, PointCh toPoint,
                   double length, DoubleUnaryOperator profile) {
    /**
     * Quick constructor that takes a graph (Graph) as an input.
     *
     * @param graph      the graph
     * @param edgeId     the ID of the edge
     * @param fromNodeId the ID of the node from which the edge is coming from
     * @param toNodeId   the ID of the node to which the edge is going to
     * @return an edge (Edge) constructed with a graph (Graph)
     */
    public static Edge of(Graph graph, int edgeId, int fromNodeId, int toNodeId) {
        return new Edge(fromNodeId, toNodeId, graph.nodePoint(fromNodeId), graph.nodePoint(toNodeId),
                graph.edgeLength(edgeId), graph.edgeProfile(edgeId));
    }

    /**
     * Computes the position (double) on the edge that is the closest to a given point (PointCh).
     *
     * @param point the give point (PointCh)
     * @return the position on the edge that is the closest to a given point (PointCh)
     */
    public double positionClosestTo(PointCh point) {
        if ((fromPoint.e() == toPoint.e() && fromPoint.n() == toPoint.n() && fromPoint.e() == point.e() && fromPoint.n() == point.n())) {
            return 0.0;
        }
        return Math2.projectionLength(fromPoint.e(), fromPoint.n(),
                toPoint.e(), toPoint.n(), point.e(), point.n());
    }

    /**
     * Computes the point(PointCh) that is placed on a given position on the edge.
     *
     * @param position the position on the edge
     * @return the point that is placed on a given position on the edge
     */
    public PointCh pointAt(double position) {

        double e = Math2.interpolate(0, (toPoint.e() - fromPoint.e())/length, position) + fromPoint.e();
        double n = Math2.interpolate(0, (toPoint.n() - fromPoint.n())/length, position) + fromPoint.n();

        return new PointCh(e, n);
    }

    /**
     * Computes the altitude (in meters) at a given position (double).
     *
     * @param position the position on the edge
     * @return the altitude (in meters) at a given position (double)
     */
    public double elevationAt(double position) {
        return profile.applyAsDouble(position);
    }
}
