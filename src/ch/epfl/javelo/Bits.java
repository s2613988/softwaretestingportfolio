package ch.epfl.javelo;

/**
 * Contains operations on bit strings.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */

public final class Bits {

    /**
     * Private constructor to prevent instantiation.
     */
    private Bits() {
    }

    /**
     * Extracts a signed bit of a given length in a bit of size 32.
     *
     * @param value  The bit we are extracting from
     * @param start  The start of the interval we are extracting
     * @param length The length of the interval
     * @return Signed bit
     * @throws IllegalArgumentException The interval isn't contained in the bit,
     *                                  the length isn't between 0(excl.) and 32(incl.) or
     *                                  start isn't between 0(incl.) and 31(incl.)
     */
    public static int extractSigned(int value, int start, int length) {
        Preconditions.checkArgument((length <= Integer.SIZE) && (length > 0)
                && ((start + length) >= 0) && ((start + length) <= Integer.SIZE) && start >= 0 && start <= Integer.SIZE - 1);

        int signedBit = value << Integer.SIZE - (start + length);
        signedBit >>= Integer.SIZE - length;

        return signedBit;
    }

    /**
     * Extracts an unsigned bit of a given length in another bit of size 32.
     *
     * @param value  The bit we are extracting from
     * @param start  The start of the interval we are extracting
     * @param length The length of the interval
     * @return Unsigned bit
     * @throws IllegalArgumentException The interval isn't contained in the bit,
     *                                  the length isn't between 0(excl.) and 32(incl.) or
     *                                  start isn't between 0(incl.) and 31(incl.)
     */
    public static int extractUnsigned(int value, int start, int length) {
        Preconditions.checkArgument((length <= Integer.SIZE - 1) && (length > 0)
                && ((start + length) >= 0) && ((start + length) <= Integer.SIZE) && start >= 0 && start <= Integer.SIZE - 1);

        int unSignedBit = value << Integer.SIZE - (start + length);
        unSignedBit >>>= Integer.SIZE - length;

        return unSignedBit;

    }
}
