package ch.epfl.javelo;

/**
 * Class which has as unique purpose to throw an IllegalArgumentException
 * if the arguments received in its methode are not valid (false).
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class Preconditions {

    /**
     * Private constructor to prevent instantiation.
     */
    private Preconditions() {
    }

    /**
     * Check the truth of an argument.
     *
     * @param shouldBeTrue is the tested argument
     * @throws IllegalArgumentException if the argument is false
     */
    public static void checkArgument(boolean shouldBeTrue) {
        if (!shouldBeTrue) {
            throw new IllegalArgumentException();
        }
    }
}
