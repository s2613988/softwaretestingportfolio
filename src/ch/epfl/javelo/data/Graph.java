package ch.epfl.javelo.data;

import ch.epfl.javelo.Functions;
import ch.epfl.javelo.projection.PointCh;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleUnaryOperator;

/**
 * Class containing all the information about the nodes, sectors, edges and their attributes.
 * This data is stocked in their respective GraphNodes, GraphSectors, GraphEdges and a List
 * of attributes that are initialized with the data of the file.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */

public final class Graph {

    private final GraphNodes nodes;
    private final GraphSectors sectors;
    private final GraphEdges edges;
    private final List<AttributeSet> attributeSets;

    /**
     * Constructor of Graph.
     *
     * @param nodes         the nodes of the Graph
     * @param sectors       the sectors of the Graph
     * @param edges         the edges of the Graph
     * @param attributeSets the List of AttributeSet of the Graph
     */
    public Graph(GraphNodes nodes, GraphSectors sectors, GraphEdges edges, List<AttributeSet> attributeSets) {
        this.nodes = nodes;
        this.sectors = sectors;
        this.edges = edges;
        this.attributeSets = List.copyOf(attributeSets);
    }

    /**
     * Creates an instance of Graph that is mapped to a file (basePath).
     *
     * @param basePath takes a base Path as an input
     * @return a new Graph with as input the respective mapped attributes
     * @throws IOException if one of the files names doesn't exist
     */
    public static Graph loadFrom(Path basePath) throws IOException {
        List<AttributeSet> attributeSets = new ArrayList<>();
        LongBuffer attributesBuffer = maping(basePath, "attributes.bin").asLongBuffer();

        for (int i = 0; i < attributesBuffer.capacity(); i++) {
            attributeSets.add(new AttributeSet(attributesBuffer.get(i)));
        }

        return new Graph(
                new GraphNodes(maping(basePath, "nodes.bin").asIntBuffer()),
                new GraphSectors(maping(basePath, "sectors.bin")),
                new GraphEdges(maping(basePath, "edges.bin"),
                        maping(basePath, "profile_ids.bin").asIntBuffer(),
                        maping(basePath, "elevations.bin").asShortBuffer()),
                attributeSets);
    }

    /**
     * Private method to create a ByteBuffer and by so to prevent redundant code in the method LoadFrom of Graph.
     *
     * @param basePath the basePath
     * @param fileName the fileName
     * @return the mapped corresponding ByteBuffer
     * @throws IOException if there is an issue at the opening or closing of the file
     */
    private static ByteBuffer maping(Path basePath, String fileName) throws IOException {
        Path filePath = basePath.resolve(fileName);
        ByteBuffer byteBuffer;
        try (FileChannel channel = FileChannel.open(filePath)) {
            byteBuffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
        }
        return byteBuffer;
    }

    /**
     * Computes the total number of nodes in the Graph.
     *
     * @return the total number of nodes in the Graph
     */
    public int nodeCount() {
        return nodes.count();
    }

    /**
     * Gives the point (PointCH) of the node associated to its nodeId (int).
     *
     * @param nodeId the ID of the node
     * @return a new PointCh equivalent to the position of the node associated with its ID (nodeId)
     */
    public PointCh nodePoint(int nodeId) {
        return new PointCh(nodes.nodeE(nodeId), nodes.nodeN(nodeId));
    }

    /**
     * Computes the number of edges going out of a given node (nodeId).
     *
     * @param nodeId ID of the given node
     * @return number of edges going out the given node
     */
    public int nodeOutDegree(int nodeId) {
        return nodes.outDegree(nodeId);
    }

    /**
     * Computes the identity of the index (edgeIndex) edge going out of a give node(nodeId).
     *
     * @param nodeId    ID of the given node
     * @param edgeIndex index of the edge going out of the given node
     * @return the identity of the index (edgeIndex) edge going out of a give node(nodeId)
     */
    public int nodeOutEdgeId(int nodeId, int edgeIndex) {
        return nodes.edgeId(nodeId, edgeIndex);
    }

    /**
     * Computes the identity of the node that is the closest to a given
     * point (PointCh) in a certain search Radius (double).
     *
     * @param point          the given point as a reference
     * @param searchDistance the search distance around the given point
     * @return the identity of the node that is the closest to a
     * given point (PointCh) in a certain search Radius (double)
     */
    public int nodeClosestTo(PointCh point, double searchDistance) {
        List<GraphSectors.Sector> searchSectors = sectors.sectorsInArea(point, searchDistance);
        double distance = searchDistance * searchDistance;
        int counter = -1;

        for (GraphSectors.Sector searchSector : searchSectors) {
            for (int i = searchSector.startNodeId(); i < searchSector.endNodeId(); i++) {

                if (nodePoint(i).squaredDistanceTo(point) < distance) {
                    counter = i;
                    distance = nodePoint(i).squaredDistanceTo(point);
                }
            }
        }
        return counter;
    }

    /**
     * Computes the identity of the target node based on the given ID
     * of an edge (edgeId).
     *
     * @param edgeId the identity of an edge
     * @return the identity of the target node
     * based on the given ID of an edge (edgeId)
     */
    public int edgeTargetNodeId(int edgeId) {
        return edges.targetNodeId(edgeId);
    }

    /**
     * Computes the boolean value (true or false) iff the given edge associated
     * to its identity (edgeId) is going to the opposite direction of the OSM
     * way from which it is based from.
     *
     * @param edgeId the identity of an edge
     * @return true if it is inverted and false if it is not
     */
    public boolean edgeIsInverted(int edgeId) {
        return edges.isInverted(edgeId);
    }

    /**
     * Computes the set of attributes (AttributeSet) associated to a given edge ID (edgeId).
     *
     * @param edgeId the identity of an edge
     * @return the set of attributes (AttributeSet) associated to a given edge ID (edgeId)
     */
    public AttributeSet edgeAttributes(int edgeId) {
        return attributeSets.get(edges.attributesIndex(edgeId));
    }

    /**
     * Computes the length (in meters) of a given edge associated to its edge ID (edgeId).
     *
     * @param edgeId the identity of an edge
     * @return the length (in meter) of a given edge ID (edgeId)
     */
    public double edgeLength(int edgeId) {
        return edges.length(edgeId);
    }

    /**
     * Computes the total positive elevation of a given edge associated to its edge ID.
     *
     * @param edgeId the identity of an edge
     * @return the total positive elevation of a given edge
     */
    public double edgeElevationGain(int edgeId) {
        return edges.elevationGain(edgeId);
    }

    /**
     * Computes the profile of a given edge associated to its edge ID.
     * If the given edge haven't got a profile associated to it, the functions computed is a constant (Double.NaN).
     *
     * @param edgeId the identity of an edge
     * @return (DoubleUnaryOperator): the profile of a given edge associated to its edge ID
     */
    public DoubleUnaryOperator edgeProfile(int edgeId) {
        return ! edges.hasProfile(edgeId) ? Functions.constant(Double.NaN) :
                Functions.sampled(edges.profileSamples(edgeId), edges.length(edgeId));
    }
}
