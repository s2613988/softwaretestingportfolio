package ch.epfl.javelo.data;

import ch.epfl.javelo.Bits;
import ch.epfl.javelo.Math2;
import ch.epfl.javelo.Q28_4;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

/**
 * Array of all the edges in a graph.
 *
 * @param edgesBuffer Attributes value for every edges
 * @param profileIds  Attributes value for every edges (different buffer for efficiency)
 * @param elevations  All elevation samples (compressed or not)
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public record GraphEdges(ByteBuffer edgesBuffer, IntBuffer profileIds, ShortBuffer elevations) {

    //OFFSET constants to make buffer access cleaner.
    private static final int OFFSET_D_ID = 0;
    private static final int OFFSET_LENGTH = OFFSET_D_ID + Integer.BYTES;
    private static final int OFFSET_ELEVATION = OFFSET_LENGTH + Short.BYTES;
    private static final int OFFSET_OSM_IDS = OFFSET_ELEVATION + Short.BYTES;
    private static final int EDGES_BYTES = OFFSET_OSM_IDS + Short.BYTES;

    private static final int EDGE_DIRECTION_FIRST_BIT = 31;
    private static final int EDGE_DIRECTION_LENGTH = 1;

    private static final int PROFILE_TYPE_FIRST_BIT = 30;
    private static final int PROFILE_TYPE_FIRST_LENGTH  = 2;
    private static final int PROFILE_ID_FIRST_BIT = 0;
    private static final int PROFILE_ID_FIRST_LENGTH  = 30;


    private static final int TYPE_1_LENGTH = 16;
    private static final int TYPE_2_LENGTH = 8;
    private static final int TYPE_3_LENGTH = 4;
    private static final int TYPE_2_SAMPLES_SHORT = TYPE_1_LENGTH / TYPE_2_LENGTH;
    private static final int TYPE_3_SAMPLES_SHORT = TYPE_1_LENGTH / TYPE_3_LENGTH;




    /**
     * Checks if an edge is inverted relative to the OSM Data.
     *
     * @param edgeId ID of the edge
     * @return True if inverted
     */
    public boolean isInverted(int edgeId) {
        return Bits.extractSigned(edgesBuffer.getInt(edgeId * EDGES_BYTES), EDGE_DIRECTION_FIRST_BIT, EDGE_DIRECTION_LENGTH) < 0;
    }

    /**
     * Gives the target-node of an edge.
     *
     * @param edgeId ID of the edge
     * @return ID of the node
     */
    public int targetNodeId(int edgeId) {
        return isInverted(edgeId) ? ~edgesBuffer.getInt(edgeId * EDGES_BYTES) : edgesBuffer.getInt(edgeId * EDGES_BYTES);
    }

    /**
     * Gives the length of an edge in meter.
     *
     * @param edgeId ID of the edge
     * @return Length of the edge
     */
    public double length(int edgeId) {
        int length = Short.toUnsignedInt(edgesBuffer.getShort(edgeId * EDGES_BYTES + OFFSET_LENGTH));
        return Q28_4.asDouble(length);
    }

    /**
     * Gives the positive elevation gain of an edge in meter.
     *
     * @param edgeId ID of the edge
     * @return Elevation gain
     */
    public double elevationGain(int edgeId) {
        int elevationGain = Short.toUnsignedInt(edgesBuffer.getShort(edgeId * EDGES_BYTES + OFFSET_ELEVATION));
        return Q28_4.asDouble(elevationGain);
    }

    /**
     * Checks if the edge has a profile.
     *
     * @param edgeId ID of the edge
     * @return True if the edge has a profile
     */
    public boolean hasProfile(int edgeId) {
        return Bits.extractUnsigned(profileIds.get(edgeId), 30, 2) != 0;
    }

    /**
     * Gives an array of all the profile samples of an edge.
     *
     * @param edgeId ID of the edge
     * @return Array of all profile samples
     */
    public float[] profileSamples(int edgeId) {

        int edgeLength = Short.toUnsignedInt(edgesBuffer.getShort(edgeId * EDGES_BYTES + OFFSET_LENGTH));
        int profileType = Bits.extractUnsigned(profileIds.get(edgeId), PROFILE_TYPE_FIRST_BIT, PROFILE_TYPE_FIRST_LENGTH);
        int firstProfileId = Bits.extractUnsigned(profileIds.get(edgeId), PROFILE_ID_FIRST_BIT, PROFILE_ID_FIRST_LENGTH);
        int nSamples = 1 + Math2.ceilDiv(edgeLength, Q28_4.ofInt(2));

        float[] samples = new float[nSamples];
        samples[0] = Q28_4.asFloat(Short.toUnsignedInt(elevations.get(firstProfileId)));

        switch (profileType) {
            case 0:
                return new float[0];

            case 1:
                for (int i = 0; i < nSamples; i++) {
                    if (i + 1 < nSamples) {
                        samples[i + 1] = Q28_4.asFloat(Short.toUnsignedInt(elevations.get(i + firstProfileId + 1)));
                    }
                }
                return isInverted(edgeId) ? reverseArray(samples) : samples;

            case 2:
                compressedProfileExtractor(samples, firstProfileId, nSamples, TYPE_2_SAMPLES_SHORT, TYPE_2_LENGTH);

                return isInverted(edgeId) ? reverseArray(samples) : samples;

            case 3:
                compressedProfileExtractor(samples, firstProfileId, nSamples, TYPE_3_SAMPLES_SHORT, TYPE_3_LENGTH);

                return isInverted(edgeId) ? reverseArray(samples) : samples;

            default: return null;
        }
    }

    private void compressedProfileExtractor(float[] samples,int firstProfileId, int nSamples, int typeSamplesShort, int typeLength){
        for (int i = 0; i < Math2.ceilDiv(nSamples, typeSamplesShort); i++) {
            for (int j = 0; j < typeSamplesShort; j++) {
                if (i * typeSamplesShort + j + 1 < nSamples) {
                    samples[i * typeSamplesShort + j + 1] = samples[i * typeSamplesShort + j] + Q28_4.asFloat(
                            Bits.extractSigned(elevations.get(i + firstProfileId + 1), typeLength * (typeSamplesShort - 1 - j), typeLength));
                }
            }
        }
    }

    /**
     * Gives the ID of the OSM attributes of an edge.
     *
     * @param edgeId ID of the edge
     * @return ID of the OSM attributes
     */
    public int attributesIndex(int edgeId) {
        return Short.toUnsignedInt(edgesBuffer.getShort(edgeId * EDGES_BYTES + OFFSET_OSM_IDS));
    }

    /**
     * Reverses a given array.
     *
     * @param floats Array
     * @return Reversed array
     */
    private float[] reverseArray(float[] floats) {
        float[] floatsClone = floats.clone();
        for (int i = 0; i < floats.length; i++) {
            floats[i] = floatsClone[floats.length - 1 - i];
        }
        return floats;
    }
}
