package ch.epfl.javelo.data;

import ch.epfl.javelo.Preconditions;

import java.util.StringJoiner;

/**
 * Set of attributes coming from the enum Attribute
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */

public record AttributeSet(long bits) {

    /**
     * Constructor of AttributeSet
     *
     * @param bits Represents the content of the set where the position of each bit
     *             corresponds to the attribute's ID in the enum
     * @throws IllegalArgumentException A bit set to 1 that doesn't correspond
     *                                  to any attribute
     */
    public AttributeSet {
        Preconditions.checkArgument(bits >>> Attribute.COUNT == 0);
    }

    /**
     * Builder of AttributeSet.
     *
     * @param attributes Attributes that are in the set
     * @return The set of attributes
     */
    public static AttributeSet of(Attribute... attributes) {
        long bits = 0L;
        long mask;
        for (Attribute attribute : attributes) {
            mask = 1L << attribute.ordinal();
            bits |= mask;
        }

        return new AttributeSet(bits);
    }

    /**
     * Checks if an attribute is in the set.
     *
     * @param attribute Attribute that is being checked
     * @return True if the set contains the attribute
     */
    public boolean contains(Attribute attribute) {
        long mask = 1L << attribute.ordinal();
        return (mask & bits) != 0;
    }

    /**
     * Checks if a set intersects with this set
     * (contains at least a common attribute).
     *
     * @param that The other set
     * @return True if they intersects
     */
    public boolean intersects(AttributeSet that) {
        return (this.bits & that.bits) != 0;
    }

    @Override
    public String toString() {
        StringJoiner string = new StringJoiner(",", "{", "}");
        for (Attribute attribute : Attribute.ALL){
            if (contains(attribute)){
                string.add(attribute.toString());
            }
        }

        return string.toString();
    }
}
