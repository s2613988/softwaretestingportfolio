package ch.epfl.javelo.data;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Array containing all the 16384 sectors in Javelo.
 *
 * @param buffer Attributes value for every sector
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public record GraphSectors(ByteBuffer buffer) {

    //OFFSET constants to make buffer access cleaner.
    private static final int OFFSET_NODE_ID = 0;
    private static final int OFFSET_NODE_NUMBER = OFFSET_NODE_ID + Integer.BYTES;
    private static final int SECTOR_BYTES = OFFSET_NODE_NUMBER + Short.BYTES;

    //Constants that gives the dimensions of a sector.
    private static final int N_SECTOR = 128;
    private static final double SECTOR_WIDTH = SwissBounds.WIDTH / N_SECTOR;
    private static final double SECTOR_HEIGHT = SwissBounds.HEIGHT / N_SECTOR;

    /**
     * Sector of Javelo.
     *
     * @param startNodeId ID of the first node in the sector
     * @param endNodeId   ID of the last node in the sector
     */
    public record Sector(int startNodeId, int endNodeId) {
    }

    /**
     * Computes the list of every sectors that intersect with the given squared area.
     *
     * @param center   Coordinates of the center of the area
     * @param distance Half of the side of a square
     * @return All sectors that intersects with the area
     */
    public List<Sector> sectorsInArea(PointCh center, double distance) {

        List<Sector> sectors = new ArrayList<>();

        // Making a coordinate system where the origin is on the bottom left edge
        // of switzerland and each unit is a sector.

        // First we make sure the East and North coordinates are within bounds
        // of Switzerland.
        // We then make the origin in the bottom left corner.
        double minE = Math2.clamp(SwissBounds.MIN_E, center.e() - distance, SwissBounds.MAX_E) - SwissBounds.MIN_E;
        double minN = Math2.clamp(SwissBounds.MIN_N, center.n() - distance, SwissBounds.MAX_N) - SwissBounds.MIN_N;
        double maxE = Math2.clamp(SwissBounds.MIN_E, center.e() + distance, SwissBounds.MAX_E) - SwissBounds.MIN_E;
        double maxN = Math2.clamp(SwissBounds.MIN_N, center.n() + distance, SwissBounds.MAX_N) - SwissBounds.MIN_N;

        //We then make every unit the size of a sector.
        int minX = (int) Math.floor(minE / SECTOR_WIDTH);
        int minY = (int) Math.floor(minN / SECTOR_HEIGHT);
        int maxX = (int) Math.floor(maxE / SECTOR_WIDTH);
        int maxY = (int) Math.floor(maxN / SECTOR_HEIGHT);

        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {

                int sectorId = y * N_SECTOR + x;
                sectors.add(new Sector(startNodeId(sectorId), endNodeId(sectorId)));
            }
        }
        return sectors;
    }

    /**
     * Gives the ID of the first node in a sector.
     *
     * @param sectorId ID of the sector
     * @return ID of the node
     */
    private int startNodeId(int sectorId) {
        return buffer.getInt(sectorId * SECTOR_BYTES + OFFSET_NODE_ID);
    }

    /**
     * Gives the ID of the node after the last node of a sector.
     *
     * @param sectorId ID of the sector
     * @return ID of the node
     */
    private int endNodeId(int sectorId) {
        return startNodeId(sectorId) + Short.toUnsignedInt(buffer.getShort(sectorId * SECTOR_BYTES + OFFSET_NODE_NUMBER));
    }
}