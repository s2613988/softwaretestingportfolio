package ch.epfl.javelo.data;

import ch.epfl.javelo.Bits;
import ch.epfl.javelo.Q28_4;

import java.nio.IntBuffer;

/**
 * GraphNodes is the array of all nodes in a graph.
 *
 * @param buffer Attributes value for every nodes
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */

public record GraphNodes(IntBuffer buffer) {

    //OFFSET constants to make buffer access cleaner
    private static final int OFFSET_E = 0;
    private static final int OFFSET_N = OFFSET_E + 1;
    private static final int OFFSET_OUT_EDGES = OFFSET_N + 1;
    private static final int NODE_INTS = OFFSET_OUT_EDGES + 1;

    private static final int OUT_EDGES_DEGREE_LENGTH = 4;
    private static final int OUT_EDGES_DEGREE_FIRST_BIT = 28;
    private static final int OUT_EDGES_ID_LENGTH = 28;
    private static final int OUT_EDGES_ID_FIRST_BIT = 0;



    /**
     * Counts the number of nodes.
     *
     * @return Number of nodes
     */
    public int count() {
        return buffer().capacity() / NODE_INTS;
    }

    /**
     * Gives the East Coordinates of a given node.
     *
     * @param nodeId ID of the node
     * @return East Coordinates
     */
    public double nodeE(int nodeId) {
        return Q28_4.asDouble(buffer().get(nodeId * NODE_INTS + OFFSET_E));
    }

    /**
     * Gives the North Coordinates of a given node.
     *
     * @param nodeId ID of the node
     * @return North Coordinates
     */
    public double nodeN(int nodeId) {
        return Q28_4.asDouble(buffer().get(nodeId * NODE_INTS + OFFSET_N));
    }

    /**
     * Gives the number of edges coming out of a given node.
     *
     * @param nodeId ID of the node
     * @return Number of edges
     */
    public int outDegree(int nodeId) {
        return Bits.extractUnsigned(buffer.get(nodeId * NODE_INTS + OFFSET_OUT_EDGES), OUT_EDGES_DEGREE_FIRST_BIT, OUT_EDGES_DEGREE_LENGTH);
    }

    /**
     * Gives the ID of the edgeIndex'th edge of a given node.
     *
     * @param nodeId    ID of the node
     * @param edgeIndex Index of the edge
     * @return ID of the edgeIndex'th edge.
     */
    public int edgeId(int nodeId, int edgeIndex) {
        return Bits.extractUnsigned(buffer.get(nodeId * NODE_INTS + OFFSET_OUT_EDGES), OUT_EDGES_ID_FIRST_BIT   , OUT_EDGES_ID_LENGTH) + edgeIndex;
    }
}
