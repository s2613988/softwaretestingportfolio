package ch.epfl.javelo.projection;

/**
 * Class that can compute transformations between the EAST-NORTH
 * system of coordinates (Ch1903) and the latitudinal and
 * longitudinal system of coordinates (WGS84).
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class Ch1903 {
    /**
     * Private constructor to prevent instantiation.
     */
    private Ch1903() {
    }

    /**
     * Converts longitudinal and latitudinal coordinates into the Swiss System of Coordinates (EAST).
     *
     * @param lon the longitudinal
     * @param lat the latitudinal
     * @return the EAST coordinates in the Swiss System
     */
    public static double e(double lon, double lat) {
        double lon1 = 1e-4 * (3600 * Math.toDegrees(lon) - 26782.5);
        double lat1 = 1e-4 * (3600 * Math.toDegrees(lat) - 169028.66);

        return 2600072.37 + 211455.93 * lon1 - 10938.51 * lon1 * lat1 - 0.36 * lon1 * Math.pow(lat1, 2) - 44.54 * Math.pow(lon1, 3);
    }

    /**
     * Converts longitudinal and latitudinal coordinates into the Swiss System of Coordinates (NORTH).
     *
     * @param lon the longitudinal
     * @param lat the latitudinal
     * @return the NORTH coordinates  in the Swiss System
     */
    public static double n(double lon, double lat) {
        double lon1 = 1e-4 * (3600 * Math.toDegrees(lon) - 26782.5);
        double lat1 = 1e-4 * (3600 * Math.toDegrees(lat) - 169028.66);

        return 1200147.07 +
                308807.95 * lat1 +
                3745.25 * lon1 * lon1 +
                76.63 * lat1 * lat1 -
                194.56 * lon1 * lon1 * lat1 +
                119.79 * Math.pow(lat1, 3);
    }

    /**
     * Converts Swiss Coordinates EAST and NORTH (e,n) into longitudinal coordinates.
     *
     * @param e the EAST coordinates
     * @param n the NORTH coordinates
     * @return the longitudinal coordinates
     */
    public static double lon(double e, double n) {
        double x = 1e-6 * (e - 2600000);
        double y = 1e-6 * (n - 1200000);
        double lon1 = 2.6779094 +
                4.728982 * x +
                0.791484 * x * y +
                0.1306 * x * y * y -
                0.0436 * Math.pow(x, 3);
        double result = (lon1 * 100) / 36;

        return Math.toRadians(result);
    }

    /**
     * Converts Swiss Coordinates EAST and NORTH (e,n) into latitudinal coordinates.
     *
     * @param e the EAST coordinates
     * @param n the NORTH coordinates
     * @return the latitudinal coordinates
     */
    public static double lat(double e, double n) {
        double x = 1e-6 * (e - 2600000);
        double y = 1e-6 * (n - 1200000);
        double lat1 = 16.9023892 +
                3.238272 * y -
                0.270978 * x * x -
                0.002528 * y * y -
                0.0447 * x * x * y -
                0.0140 * Math.pow(y, 3);
        double result = (lat1 * 100) / 36;

        return Math.toRadians(result);
    }
}
