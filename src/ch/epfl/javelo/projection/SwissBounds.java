package ch.epfl.javelo.projection;

/**
 * Describes the approximate limits of Swiss in the EAST-NORTH system of coordinates,
 * the origin of the coordinates (E = 2600000, N = 1200000) is in Bern.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class SwissBounds {

    /**
     * Private constructor to prevent instantiation.
     */
    private SwissBounds() {
    }

    /**
     * SwissBounds constants.
     */
    public static final double MIN_E = 2485000;
    public static final double MAX_E = 2834000;
    public static final double MIN_N = 1075000;
    public static final double MAX_N = 1296000;
    public static final double WIDTH = MAX_E - MIN_E;
    public static final double HEIGHT = MAX_N - MIN_N;

    /**
     * Tells if coordinates belong to the bounds of Swiss.
     *
     * @param e EAST coordinates
     * @param n NORTH coordinates
     * @return true if the coordinates are inside Switzerland
     */
    public static boolean containsEN(double e, double n) {
        return e >= MIN_E && e <= MAX_E && n >= MIN_N && n <= MAX_N;
    }
}
