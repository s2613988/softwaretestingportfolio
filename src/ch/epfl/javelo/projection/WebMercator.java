package ch.epfl.javelo.projection;

import ch.epfl.javelo.Math2;

/**
 * Class that allows to compute conversions going from the WGS 84 system
 * of coordinates to the Web Mercator system of coordinates.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class WebMercator {

    /**
     * Private constructor to prevent instantiation.
     */
    private WebMercator() {
    }

    /**
     * Gives the coordinate x in the WebMercator System of coordinates.
     *
     * @param lon, longitudinal coordinates given in radians
     * @return the coordinate x in the WebMercator System of coordinates
     */
    public static double x(double lon) {
        return 1 / (2 * Math.PI) * (lon + Math.PI);
    }

    /**
     * Gives the coordinate y in the WebMercator System of coordinates.
     *
     * @param lat, latitudinal coordinates given in radians
     * @return the coordinate x in the WebMercator System of coordinates
     */
    public static double y(double lat) {
        return 1 / (2 * Math.PI) * (Math.PI - Math2.asinh(Math.tan(lat)));
    }

    /**
     * Gives the longitudinal coordinate in the global System of coordinate.
     *
     * @param x X coordinate in the WebMercator System of coordinates
     * @return gives the longitudinal coordinate in the global System of coordinate
     */
    public static double lon(double x) {
        return 2 * Math.PI * x - Math.PI;
    }

    /**
     * Gives the latitudinal coordinate in the global System of coordinate.
     *
     * @param y Y coordinate in the WebMercator System of coordinates
     * @return the latitudinal coordinate in the global System of coordinate
     */
    public static double lat(double y) {
        return Math.atan(Math.sinh(Math.PI - 2 * Math.PI * y));
    }
}
