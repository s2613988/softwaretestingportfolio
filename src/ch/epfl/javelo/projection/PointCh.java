package ch.epfl.javelo.projection;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.Preconditions;

/**
 * Represents a geographic point, in the EAST-NORTH System of coordinates.
 *
 * @param e the EAST coordinates
 * @param n the NORTH coordinates
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public record PointCh(double e, double n) {
    /**
     * Constructor of PointCh to test conditions.
     *
     * @param e East coordinates
     * @param n North coordinates
     * @throws IllegalArgumentException if preconditions are not respected (has to be in the Swiss bound).
     */
    public PointCh {
        Preconditions.checkArgument(SwissBounds.containsEN(e, n));
    }

    /**
     * Computes the square of the distance from this (PointCH) to that (PointCH).
     *
     * @param that target coordinates
     * @return the squared distance
     */
    public double squaredDistanceTo(PointCh that) {
        double distanceE = this.e - that.e;
        double distanceN = this.n - that.n;

        return Math2.squaredNorm(distanceE, distanceN);
    }

    /**
     * Computes  the distance from this (PointCH) to that (PointCH).
     *
     * @param that target coordinates
     * @return the distance from this(PointCH) to that (PointCh)
     */
    public double distanceTo(PointCh that) {
        return Math.sqrt(squaredDistanceTo(that));
    }

    /**
     * Converts the coordinates of the point to longitudinal unity in the WGS84 system.
     *
     * @return the longitude of the coordinates in the PointCh
     */
    public double lon() {
        return Ch1903.lon(this.e, this.n);
    }

    /**
     * Converts the coordinates of the point to latitudinal unity in the WGS84 system.
     *
     * @return the latitude of the coordinates in the PointCh
     */
    public double lat() {
        return Ch1903.lat(this.e, this.n);
    }

}
