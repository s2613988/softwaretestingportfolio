package ch.epfl.javelo.projection;

import ch.epfl.javelo.Preconditions;

/**
 * Class that can compute mathematical manipulations on the WebMercator
 * points and correspondences between the WebMercator points and the
 * Swiss point (PointCh).
 *
 * @param x the X coordinate
 * @param y the Y coordinate
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public record PointWebMercator(double x, double y) {
    public final static int MINIMAL_ZOOM_LEVEL = 8;

    /**
     * Compact constructor that verifies that the points x and y is in between 0 and 1.
     *
     * @param x X coordinates
     * @param y Y coordinates
     */
    public PointWebMercator {
        Preconditions.checkArgument(x >= 0 && x <= 1 && y >= 0 && y <= 1);
    }

    /**
     * Creates a PointWebMercator with the zoomed coordinates in the constructor.
     *
     * @param zoomLevel value of the zoom level
     * @param x         X coordinate in the WebMercator System of coordinates
     * @param y         Y coordinate in the WebMercator System of coordinates
     * @return (PointWebMercator): with the zoomed coordinates
     */
    public static PointWebMercator of(int zoomLevel, double x, double y) {
        double xZoomed = Math.scalb(x, -MINIMAL_ZOOM_LEVEL - zoomLevel);
        double yZoomed = Math.scalb(y, -MINIMAL_ZOOM_LEVEL - zoomLevel);

        return new PointWebMercator(xZoomed, yZoomed);
    }

    /**
     * Creates a PointWebMercator with the same coordinates as a PointCh.
     *
     * @param pointCh the pointCh
     * @return a PointWebMercator with the same coordinates as the pointCh
     */
    public static PointWebMercator ofPointCh(PointCh pointCh) {
        double x = WebMercator.x(pointCh.lon());
        double y = WebMercator.y(pointCh.lat());

        return new PointWebMercator(x, y);
    }

    /**
     * Computes the x coordinate in the WebMercator System of coordinates at
     * a certain zoom level.
     *
     * @param zoomLevel the level of the zoom
     * @return the value of coordinates x in the WebMercator System of coordinates
     * at a certain zoom level (zoomLevel)
     */
    public double xAtZoomLevel(int zoomLevel) {
        return Math.scalb(this.x, MINIMAL_ZOOM_LEVEL + zoomLevel);
    }

    /**
     * Computes the y coordinate in the WebMercator System of coordinates
     * at a certain zoom level.
     *
     * @param zoomLevel the level of the zoom
     * @return the value of coordinates y in the WebMercator System of coordinates at
     * a certain zoom level (zoomLevel)
     */
    public double yAtZoomLevel(int zoomLevel) {
        return Math.scalb(this.y, MINIMAL_ZOOM_LEVEL + zoomLevel);
    }

    /**
     * Gives the longitudinal value (in radians) of this (PointWebMercator)
     * in the WSG-84 System of Coordinates.
     *
     * @return the longitudinal value (in radians) in the WSG-84 System of coordinates
     */
    public double lon() {
        return WebMercator.lon(this.x);
    }

    /**
     * Gives the latitudinal value (in radians) of this (PointWebMercator)
     * in the WSG-84 System of Coordinates.
     *
     * @return the latitudinal value (in radians) in the WSG-84 System of coordinates
     */
    public double lat() {
        return WebMercator.lat(this.y);
    }

    /**
     * Converts this(PointWebMercator) into a PointCh with the same coordinates.
     *
     * @return a PointCh with the same coordinates as this (PointWebMercator)
     */
    public PointCh toPointCh() {
        double e = Ch1903.e(lon(), lat());
        double n = Ch1903.n(lon(), lat());

        return SwissBounds.containsEN(e, n) ? new PointCh(e, n) : null;
    }

}
