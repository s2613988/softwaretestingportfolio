package ch.epfl.javelo;

import java.util.Arrays;
import java.util.function.DoubleUnaryOperator;

/**
 * Class that creates objects that can model mathematical
 * functions going from R to R.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class Functions {
    /**
     * Private constructor to prevent instantiation.
     */
    private Functions() {
    }

    /**
     * Creates an object Constant that has y(double) in the constructor.
     * Constant has a method @applyDouble that outputs the constant
     * y(double) for any input.
     *
     * @param y value of the constant
     * @return a private nested class (Constant) constructed with the value y(double)
     */
    public static DoubleUnaryOperator constant(double y) {
        return new Constant(y);
    }

    /**
     * Creates an object Sampled that has samples(float[]) and xMax(double)
     * in the constructor.
     * Sampled has a method @applyDouble that outputs the interpolated value
     * y(double) based on the samples(float[] samples) and xMax(double) values
     * for any input (double).
     *
     * @param samples Array of samples on which the interpolation will be based
     * @param xMax    Maximum x value for the samples
     * @return private nested class (Sampled) constructed with
     * samples(float[]) and xMax(double)
     * @throws IllegalArgumentException if preconditions are not respected :
     *                                  the number of samples should be equal
     *                                  or bigger than 2 and the xMax value should
     *                                  be strictly bigger than 0
     */
    public static DoubleUnaryOperator sampled(float[] samples, double xMax) {
        Preconditions.checkArgument(samples.length >= 2 && xMax > 0);

        return new Sampled(samples, xMax);
    }

    /**
     * Nested class Constant.
     */
    private static final class Constant implements DoubleUnaryOperator {

        private final double constant;

        /**
         * Constructor.
         *
         * @param constant the constant
         */
        private Constant(double constant) {
            this.constant = constant;
        }

        /**
         * Outputs the constant for any given input y(double).
         *
         * @param y the given input of the function
         * @return the constant output
         */
        @Override
        public double applyAsDouble(double y) {
            return constant;
        }
    }

    /**
     * Nested class Sampled.
     */
    private static final class Sampled implements DoubleUnaryOperator {

        private final float[] samples;
        private final double xMax;

        /**
         * Constructor.
         *
         * @param samples Array of samples.
         * @param xMax    Maximum x value of the samples
         */
        private Sampled(float[] samples, double xMax) {
            this.samples = Arrays.copyOf(samples, samples.length);
            this.xMax = xMax;
        }

        /**
         * Mathematical function that computes the interpolated value
         * y(double) based on
         * the samples(float[] samples) and the maximum value of the samples
         * xMax(double) for any given input (double).
         *
         * @param x the given input of the function.
         * @return the y value for the input x
         */
        @Override
        public double applyAsDouble(double x) {
            double xProv = Math2.clamp(0, x, xMax);
            double gap = xMax / (samples.length - 1);
            int indexMin = (int) Math.floor(xProv / gap);
            int indexMax = (int) Math.ceil(xProv / gap);
            double xFinal = xProv / gap - indexMin;

            if (xProv == 0) {
                return samples[0];
            } else if (xProv == xMax) {
                return samples[samples.length - 1];
            }

            return Math2.interpolate(samples[indexMin], samples[indexMax], xFinal);
        }
    }
}
