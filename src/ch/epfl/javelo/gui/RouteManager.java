package ch.epfl.javelo.gui;


import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.PointWebMercator;
import ch.epfl.javelo.routing.Route;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polyline;

/**
 * Class that is the graphic representation of an itinerary.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */

public final class RouteManager {

    private final RouteBean routebean;
    private final ReadOnlyObjectProperty<MapViewParameters> parameters;
    private final ObservableList<Waypoint> waypoints;
    private final DoubleProperty highlightedPosition;
    private final ReadOnlyObjectProperty<Route> route;

    private final Pane pane;
    private final Polyline polyline;
    private final Circle circle;

    private static final int INSERT_MIDWAY = 1;
    private static final int CIRCLE_RADIUS = 5;
    private static final String CIRCLE_ID = "highlight";
    private static final String POLYLINE_ID = "route";

    /**
     * Constructor
     *
     * @param routebean  the routeBean
     * @param parameters the MapViewParameters as an ReadOnlyObjectProperty
     */
    public RouteManager(RouteBean routebean, ReadOnlyObjectProperty<MapViewParameters> parameters) {
        this.routebean = routebean;
        this.parameters = parameters;
        this.waypoints = routebean.waypoints();

        this.pane = new Pane();
        this.polyline = new Polyline();
        this.circle = new Circle(CIRCLE_RADIUS);
        this.highlightedPosition = routebean.highlightedPositionProperty();
        this.route = routebean.routeProperty();

        polyline.setId(POLYLINE_ID);
        circle.setId(CIRCLE_ID);

        pane.getChildren().addAll(polyline, circle);
        pane.setPickOnBounds(false);


        circleManager();
        mapViewParametersListeners();
        wayPointsListeners();
        highlightedPositionListeners();

    }

    /**
     * Creates the pane that contains the graphic representation of
     * the route (polyline) and its highlighted position (circle).
     *
     * @return the pane
     */
    public Pane pane() {
        return pane;
    }

    /**
     * Sets the polyline.
     */
    private void setPolyline() {
        polyline.getPoints().clear();
        polyline.setVisible(false);

        if (route.get() != null) {
            for (PointCh pointCh : route.get().points()) {

                /* Adds the x coordinates of the current point to the polyline */
                polyline.getPoints().add(PointWebMercator.ofPointCh(pointCh).
                        xAtZoomLevel(parameters.get().zoomLevel()));

                /* Adds the Y coordinate of the current point to the polyline */
                polyline.getPoints().add(PointWebMercator.ofPointCh(pointCh).
                        yAtZoomLevel(parameters.get().zoomLevel()));
            }
            setPolylineLayouts();
            polyline.setVisible(true);
        }
    }

    /**
     * Sets the layout of the polyline.
     */
    private void setPolylineLayouts() {
        polyline.setLayoutX(-parameters.get().x());
        polyline.setLayoutY(-parameters.get().y());
    }

    /**
     * Sets the circle.
     */
    private void setCircle() {
        circle.setVisible(false);
        if (route.get() != null && !Double.isNaN(highlightedPosition.get())) {
            PointCh pointCh = route.get().pointAt(highlightedPosition.get());

            /* Sets the layout coordinates of the circle */
            circle.setLayoutX(parameters.get().viewX(PointWebMercator.ofPointCh(pointCh)));
            circle.setLayoutY(parameters.get().viewY(PointWebMercator.ofPointCh(pointCh)));

            circle.setVisible(true);
        }
    }

    /**
     * Adds the listener to the mapViewParameters, if only the position changes and not the zoom,
     * it sets the polyline properly (and doesn't recreate it) else wise
     * it sets the polyline and the circle with the new value
     * of the MapViewParameters.
     */
    private void mapViewParametersListeners() {
        parameters.addListener((propertyChanged, pastValue, newValue) -> {
            if (pastValue.zoomLevel() == newValue.zoomLevel()) {
                setPolylineLayouts();
            } else {
                setPolyline();
            }
            setCircle();
        });
    }

    /**
     * Adds the listener to the waypoints of the RouteBean. If the list of waypoints changes
     * it updates the value of the route, and resets the polyline and the circle.
     */
    private void wayPointsListeners() {
        waypoints.addListener((ListChangeListener<? super Waypoint>) change -> {
            setPolyline();
            setCircle();
        });
    }


    /**
     * Adds the listener to the highlighted position of the routeBean. If it changes
     * it updates the value of the attribute highlightedPosition and resets the polyline and the circle.
     */
    private void highlightedPositionListeners() {
        highlightedPosition.addListener((o, ov, nv) -> setCircle());
    }

    /**
     * Manages the events of the circle (highlighted position) when clicked on. If so, it creates a new waypoint.
     */
    private void circleManager() {
        circle.setOnMouseClicked(mouseEvent -> {
            Point2D point2D = circle.localToParent(mouseEvent.getX(), mouseEvent.getY());
            PointWebMercator pointWebMercator = parameters.get().pointAt(point2D.getX(), point2D.getY());
            int nodeID = route.get().nodeClosestTo(highlightedPosition.get());
            Waypoint waypoint = new Waypoint(pointWebMercator.toPointCh(), nodeID);
            waypoints.add(routebean.indexOfNonEmptySegmentAt(highlightedPosition.get()) + INSERT_MIDWAY, waypoint);
        });
    }

}
