package ch.epfl.javelo.gui;


import ch.epfl.javelo.routing.*;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.util.Pair;


import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * JavaFX bean of a route
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class RouteBean {

    private final RouteComputer routeComputer;
    private final Map<Pair<Integer, Integer>, Route> routeCache = new LruCacheMap<>(CACHE_SIZE);

    private final ObservableList<Waypoint> waypoints;
    private final ObjectProperty<Route> route;
    private final DoubleProperty highlightedPosition;
    private final ObjectProperty<ElevationProfile> elevationProfile;

    private static final double MAX_STEP_LENGTH = 5;
    private static final int CACHE_SIZE = 20;

    /**
     * Constructor of RouteBean.
     *
     * @param routeComputer route computer of the bean
     */
    public RouteBean(RouteComputer routeComputer) {

        this.routeComputer = routeComputer;
        waypoints = FXCollections.observableArrayList();
        elevationProfile = new SimpleObjectProperty<>(null);
        route = new SimpleObjectProperty<>();
        highlightedPosition = new SimpleDoubleProperty(Double.NaN);

        //Listeners Setup
        waypoints.addListener((ListChangeListener<Waypoint>) change -> UpdateRoute());
        route.addListener((o, oV, nV) -> UpdateElevationProfile());
    }

    /**
     * Update the route property.
     * Called when the waypoints List changes.
     */
    private void UpdateRoute() {

        List<Route> routes = new LinkedList<>();
        boolean isSegmentNull = false;

        for (int i = 0; i < waypoints.size() - 1; i++) {
            int startNodeId = waypoints.get(i).closestNodeId();
            int endNodeId = waypoints.get(i + 1).closestNodeId();
            Pair<Integer, Integer> waypointPair = new Pair<>(startNodeId, endNodeId);

            if (!routeCache.containsKey(waypointPair)) {
                if (startNodeId == endNodeId) {
                    continue;
                }
                Route route = routeComputer.bestRouteBetween(startNodeId, endNodeId);
                isSegmentNull = (route == null);

                if (isSegmentNull) {
                    break;
                }
                routeCache.put(waypointPair, route);
            }
            routes.add(routeCache.get(waypointPair));
        }
        if (!isSegmentNull && routes.size() > 0) {
            route.setValue(routes.size() == 1 ? routes.get(0) : new MultiRoute(routes));
        } else {
            route.setValue(null);
            elevationProfile.setValue(null);
        }
    }

    /**
     * Updates the elevationProfile property. Called when the routeProperty changes.
     */
    private void UpdateElevationProfile() {
        if (route.get() != null) {
            elevationProfile.setValue(ElevationProfileComputer.elevationProfile(route.get(), MAX_STEP_LENGTH));
        }
    }

    //Getter / Setter

    /**
     * HighlightedPosition's property getter.
     *
     * @return the property of the highlightedPosition
     */
    public DoubleProperty highlightedPositionProperty() {
        return highlightedPosition;
    }

    /**
     * Route's property getter.
     *
     * @return Read-only property of the route
     */
    public ReadOnlyObjectProperty<Route> routeProperty() {
        return route;
    }

    /**
     * elevationProfile's property getter.
     *
     * @return Read-only property of the elevationProfile
     */
    public ReadOnlyObjectProperty<ElevationProfile> elevationProfileProperty() {
        return elevationProfile;
    }

    /**
     * Waypoints's property getter.
     *
     * @return property of waypoints
     */
    public ObservableList<Waypoint> waypoints() {
        return this.waypoints;
    }


    /**
     * Gives the index of the segment containing the given position on an itinerary and
     * ignores the segments that are empty.
     *
     * @param position the given position on the itinerary
     * @return the index of the segment containing that position
     */
    public int indexOfNonEmptySegmentAt(double position) {
        int index = route.get().indexOfSegmentAt(position);
        for (int i = 0; i <= index; i += 1) {
            int n1 = waypoints.get(i).closestNodeId();
            int n2 = waypoints.get(i + 1).closestNodeId();
            if (n1 == n2) index += 1;
        }
        return index;
    }


}
