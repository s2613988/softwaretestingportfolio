package ch.epfl.javelo.gui;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.PointWebMercator;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.SVGPath;

import java.util.function.Consumer;

/**
 * Manages the graphic representation and the interactions with the way points.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */

public final class WaypointsManager {

    private final Graph graph;
    private final ObjectProperty<MapViewParameters> parameters;
    private final ObservableList<Waypoint> wayPoints;
    private final Consumer<String> error;
    private final Pane pane;

    private static final int SEARCH_DISTANCE = 1000;
    private static final int NOT_EXIST = -1;
    private static final String ERROR_MESSAGE = "Aucune route à proximité !";
    private static final String MARKER_STYLE = "pin";
    private static final String FIRST_WAYPOINT = "first";
    private static final String MIDDLE_WAYPOINT = "middle";
    private static final String LAST_WAYPOINT = "last";
    private static final String OUTSIDE_PATH = "M-8-20C-5-14-2-7 0 0 2-7 5-14 8-20 20-40-20-40-8-20";
    private static final String OUTSIDE_STYLE = "pin_outside";
    private static final String INSIDE_PATH = "M0-23A1 1 0 000-29 1 1 0 000-23";
    private static final String INSIDE_STYLE = "pin_inside";


    /**
     * Constructor
     *
     * @param graph      the Graph
     * @param parameters the MapViewParameters property
     * @param wayPoints  the observable list of waypoints
     * @param error      the error consumer
     */
    public WaypointsManager(Graph graph, ObjectProperty<MapViewParameters> parameters, ObservableList<Waypoint> wayPoints, Consumer<String> error) {
        this.graph = graph;
        this.parameters = parameters;
        this.wayPoints = wayPoints;
        this.pane = new Pane();
        this.error = error;

        WayPointsListeners();
        mapViewParametersListener();
        fillPane();
    }

    /**
     * Returns a pane that contains all the waypoints.
     *
     * @return a pane that contains all the waypoints
     */
    public Pane pane() {
        return pane;
    }


    /**
     * Adds a new waypoint with as input the coordinates relative to the MapViewParameters.
     *
     * @param xRelative the X coordinate relative to the TOP-LEFT corner
     * @param yRelative the Y coordinate relative to the TOP-LEFT corner
     */
    public void addWaypoint(double xRelative, double yRelative) {
        Waypoint wayPoint = createWayPoint(xRelative, yRelative);
        if (wayPoint != null) {
            wayPoints.add(wayPoint);
        }
    }

    /**
     * Fills the pane with all the waypoints.
     */
    private void fillPane() {
        pane.setPickOnBounds(false);
        pane.getChildren().clear();

        int index = 0;
        for (Waypoint w : wayPoints) {
            Group marker = marker();
            marker.getStyleClass().add(MARKER_STYLE);

            if (index == 0) {
                marker.getStyleClass().add(FIRST_WAYPOINT);
            } else if (index == wayPoints.size() - 1) {
                marker.getStyleClass().add(LAST_WAYPOINT);
            } else {
                marker.getStyleClass().add(MIDDLE_WAYPOINT);
            }

            fillCoordinates(marker, w);
            eventMarkerMoved(marker, w);
            pane.getChildren().add(marker);
            index++;
        }
    }

    /**
     * Creates a waypoint with as input the coordinates relative to the MapViewParameters.
     *
     * @param xRelative the X coordinate relative to the TOP-LEFT corner of the MapViewParameters
     * @param yRelative the Y coordinate relative to the TOP-LEFT corner of the MapViewParameters
     * @return a new Waypoint if the corresponding PointCh to the Waypoint
     * exists in the graph or null if the PointCh doesn't exist
     * or there is no Node near the relative coordinates.
     */
    private Waypoint createWayPoint(double xRelative, double yRelative) {
        PointCh pointCh = parameters.get().pointAt(xRelative, yRelative).toPointCh();
        if (pointCh != null) {
            int nodeClosestToID = graph.nodeClosestTo(pointCh, SEARCH_DISTANCE);
            if (nodeClosestToID == NOT_EXIST) {
                throwError();
                return null;
            }
            return new Waypoint(pointCh, nodeClosestToID);
        }
        throwError();
        return null;
    }

    /**
     * The error consumer consumes the error message if called.
     */
    private void throwError() {
        error.accept(ERROR_MESSAGE);
    }

    /**
     * Creates the Group corresponding to the graphic representation of a Waypoint marker.
     *
     * @return the Waypoint marker
     */
    private Group marker() {
        SVGPath outside = new SVGPath();
        SVGPath inside = new SVGPath();

        outside.setContent(OUTSIDE_PATH);
        outside.getStyleClass().add(OUTSIDE_STYLE);
        inside.setContent(INSIDE_PATH);
        inside.getStyleClass().add(INSIDE_STYLE);

        return new Group(outside, inside);
    }

    /**
     * Fills the coordinates on the panel of the marker corresponding to its Waypoint.
     *
     * @param marker   the marker
     * @param waypoint the corresponding Waypoint
     */
    private void fillCoordinates(Group marker, Waypoint waypoint) {
        PointWebMercator pointWebMercator = PointWebMercator.ofPointCh(waypoint.wayPointCh());
        marker.setLayoutX(parameters.get().viewX(pointWebMercator));
        marker.setLayoutY(parameters.get().viewY(pointWebMercator));
    }

    /**
     * Is the event manager of a marker when dragged or clicked on.
     *
     * @param marker   the marker
     * @param waypoint its corresponding Waypoint
     */
    private void eventMarkerMoved(Group marker, Waypoint waypoint) {
        /* Moves the marker and its corresponding Waypoint when dragged on. */
        marker.setOnMouseDragged(event -> {
            marker.setLayoutX(event.getSceneX());
            marker.setLayoutY(event.getSceneY());

            marker.setOnMouseReleased(released -> {
                Waypoint newWaypoint = createWayPoint(event.getSceneX(), event.getSceneY());
                if (newWaypoint != null) {
                    wayPoints.set(wayPoints.indexOf(waypoint), newWaypoint);
                } else {
                    fillPane();
                }
            });
        });

        /* Removes the marker and its corresponding Waypoint when clicked on.*/
        marker.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.isStillSincePress()) {
                pane.getChildren().remove(marker);
                wayPoints.remove(waypoint);
            }
        });
    }

    /**
     * Adds the listener to the wayPoints list.
     * If the list changes, then the pane is refilled with the new list of waypoints.
     */
    private void WayPointsListeners() {
        wayPoints.addListener((ListChangeListener<? super Waypoint>) change -> fillPane());
    }

    /**
     * Adds the listener to the mapViewParameters. If one of its property changes all
     * the markers associated to their waypoints are relocated in the right coordinates.
     */
    private void mapViewParametersListener() {
        parameters.addListener((v, oV, nV) -> {
            int index = 0;
            for (Node n : pane.getChildren()) {
                fillCoordinates((Group) n, wayPoints.get(index));
                index++;
            }
        });

    }
}
