package ch.epfl.javelo.gui;

import javafx.animation.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * Error Manager of Javelo
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class ErrorManager {

    private final Pane pane;
    private final Text errorMessage;
    private final Transition animationTransition;

    private static final String STYLESHEET = "error.css";
    private static final double FADE_ALPHA = 0.8;
    private static final Duration FADE_DURATION = new Duration(200);
    private static final Duration PAUSE_DURATION = new Duration(2000);


    /**
     * Constructor of ErrorManager
     */
    ErrorManager() {
        errorMessage = new Text();
        pane = new VBox(errorMessage);
        pane.getStylesheets().add(STYLESHEET);
        pane.setMouseTransparent(true);

        animationTransition = animationSetup();
    }

    /**
     * Pane of the manager.
     *
     * @return pane
     */
    public Pane pane() {
        return pane;
    }

    /**
     * Displays an error with animation.
     *
     * @param errorMessage message of the error
     */
    public void displayError(String errorMessage) {
        java.awt.Toolkit.getDefaultToolkit().beep();

        if (animationTransition.getStatus() != Animation.Status.STOPPED) {
            animationTransition.stop();
        }
        this.errorMessage.setText(errorMessage);
        animationTransition.play();
    }

    /**
     * Setup the error animation.
     */
    private Transition animationSetup() {
        FadeTransition appearance = new FadeTransition(FADE_DURATION, pane);
        appearance.setFromValue(0);
        appearance.setToValue(FADE_ALPHA);

        PauseTransition pause = new PauseTransition(PAUSE_DURATION);

        FadeTransition disappearance = new FadeTransition(FADE_DURATION, pane);
        disappearance.setFromValue(FADE_ALPHA);
        disappearance.setToValue(0);

        return new SequentialTransition(appearance, pause, disappearance);
    }
}
