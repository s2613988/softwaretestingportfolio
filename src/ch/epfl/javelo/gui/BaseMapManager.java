package ch.epfl.javelo.gui;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.projection.PointWebMercator;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;

/**
 * Manage the map of Javelo
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class BaseMapManager {

    private final TileManager tileManager;
    private final ObjectProperty<MapViewParameters> properties;
    private final WaypointsManager waypointsManager;
    private final Canvas canvas;
    private final Pane pane;

    private static final int MAX_ZOOM = 19;
    private static final int MIN_ZOOM = 8;

    private static final int TILE_SIZE = 256;
    private final int ZOOM_DELAY = 200;

    private boolean redrawNeeded = false;
    private Point2D previousMousePos;

    /**
     * Constructor of BaseMapManager.
     *
     * @param tileManager      tile manager
     * @param waypointsManager waypoints manager
     * @param properties       properties of the map
     */
    public BaseMapManager(TileManager tileManager, WaypointsManager waypointsManager, ObjectProperty<MapViewParameters> properties) {
        this.tileManager = tileManager;
        this.waypointsManager = waypointsManager;
        this.properties = properties;

        canvas = new Canvas();
        pane = new Pane(canvas);

        paneSetup();
        canvasSetup();
    }

    /**
     * Pane of the manager.
     *
     * @return pane
     */
    public Pane pane() {
        redrawOnNextPulse();
        return pane;
    }

    /**
     * Requests a redrawing of the canvas on next frame.
     */
    private void redrawOnNextPulse() {
        redrawNeeded = true;
        Platform.requestNextPulse();
    }

    /**
     * Redraws the canvas if it was requested.
     */
    private void redrawIfNeeded() {
        if (!redrawNeeded) return;
        redrawNeeded = false;
        int minX = Math.floorDiv((int) properties.get().topLeft().getX(), TILE_SIZE);
        int minY = Math.floorDiv((int) properties.get().topLeft().getY(), TILE_SIZE);
        int maxX = Math.floorDiv((int) (properties.get().x() + (int) canvas.getWidth()), TILE_SIZE);
        int maxY = Math.floorDiv((int) (properties.get().y() + (int) canvas.getHeight()), TILE_SIZE);
        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                canvas.getGraphicsContext2D().drawImage(
                        tileManager.imageForTileAt(new TileManager.TileId(properties.get().zoomLevel(), x, y)),
                        properties.get().viewX(PointWebMercator.of(properties.get().zoomLevel(), x * TILE_SIZE, y * TILE_SIZE)),
                        properties.get().viewY(PointWebMercator.of(properties.get().zoomLevel(), x * TILE_SIZE, y * TILE_SIZE)));
            }
        }
    }

    /**
     * Setup the different bind / listeners off the pane.
     */
    private void paneSetup() {

        //zoom
        SimpleLongProperty minScrollTime = new SimpleLongProperty();
        pane.setOnScroll(event -> {
            Point2D marker = new Point2D(event.getX(), event.getY());

            if (event.getDeltaY() == 0d) return;
            long currentTime = System.currentTimeMillis();
            if (currentTime < minScrollTime.get()) return;
            minScrollTime.set(currentTime + ZOOM_DELAY);
            int zoomDelta = (int) Math.signum(event.getDeltaY());

            moveMap(marker);
            zoomMap(zoomDelta);
            moveMap(marker.subtract(marker.add(marker)));
        });

        //Map Movement
        pane.setOnMousePressed(event -> previousMousePos = new Point2D(event.getX(), event.getY()));

        pane.setOnMouseDragged(event -> {
            Point2D deltaXY = previousMousePos.subtract(event.getX(), event.getY());
            previousMousePos = new Point2D(event.getX(), event.getY());
            moveMap(deltaXY);
        });
        pane.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.isStillSincePress()) {
                waypointsManager.addWaypoint(mouseEvent.getX(), mouseEvent.getY());
            }
        });

    }

    /**
     * Setup the different bind / listeners off the canvas.
     */
    private void canvasSetup() {
        //Canvas updates
        canvas.sceneProperty().addListener((p, oldS, newS) -> {
            assert oldS == null;
            newS.addPreLayoutPulseListener(this::redrawIfNeeded);
        });

        //Bindings
        canvas.widthProperty().bind(pane.widthProperty());
        canvas.heightProperty().bind(pane.heightProperty());

        //Canvas's listeners
        canvas.widthProperty().addListener((o, oV, nV) -> redrawOnNextPulse());
        canvas.heightProperty().addListener((o, oV, nV) -> redrawOnNextPulse());

        properties.addListener((o, oV, nV) -> redrawOnNextPulse());
    }

    /**
     * Moves the map.
     *
     * @param deltaXY Movement requested in (x,y) coordinates
     */
    private void moveMap(Point2D deltaXY) {
        properties.setValue(properties.get().withMinXY(properties.get().x() + (int) deltaXY.getX(), properties.get().y() + (int) deltaXY.getY()));
    }

    /**
     * Zooms on the map.
     *
     * @param deltaZoom value of the zoom
     */
    private void zoomMap(int deltaZoom) {
        int minDeltaZoom = MIN_ZOOM - properties.get().zoomLevel();
        int maxDeltaZoom = MAX_ZOOM - properties.get().zoomLevel();
        int clampedDeltaZoom = Math2.clamp(minDeltaZoom, deltaZoom, maxDeltaZoom);
        properties.setValue(new MapViewParameters(properties.get().zoomLevel() + clampedDeltaZoom,
                (int) (properties.get().x() * Math.pow(2, clampedDeltaZoom)),
                (int) (properties.get().y() * Math.pow(2, clampedDeltaZoom))));
    }


}

