package ch.epfl.javelo.gui;

import ch.epfl.javelo.routing.ElevationProfile;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.*;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Affine;
import javafx.scene.transform.NonInvertibleTransformException;
import javafx.scene.transform.Transform;

/**
 * Class that is the graphic representation of the profile of an itinerary.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class ElevationProfileManager {

    private final ReadOnlyObjectProperty<ElevationProfile> elevationProfile;
    private final ReadOnlyDoubleProperty highlightedPosition;
    private final Pane pane;
    private final ObjectProperty<Rectangle2D> rectangle;
    private final DoubleProperty mousePosition;
    private final ObjectProperty<Transform> screenToWorld;
    private final ObjectProperty<Transform> worldToScreen;
    private final Line highlightedLine;
    private final Path grille;
    private final BorderPane borderPane;
    private final VBox vbox;
    private final Polygon polygon;
    private final Group text;

    private static final String BORDER_PANE_STYLESHEET = "elevation_profile.css";
    private static final String VBOX_ID = "profile_data";
    private static final String GRILLE_ID = "grid";

    private static final Rectangle2D DEFAULT_SIZE_RECTANGLE = new Rectangle2D(0.0, 0.0, 0.0, 0.0);
    private static final Insets INSETS = new Insets(10, 10, 20, 40);
    private static final int[] POS_STEPS = {1000, 2000, 5000, 10_000, 25_000, 50_000, 100_000};
    private static final int[] ELE_STEPS = {5, 10, 20, 25, 50, 100, 200, 250, 500, 1_000};
    private static final double MIN_SCREEN_STEP_X = 50;
    private static final double MIN_SCREEN_STEP_Y = 25;
    private static final int TO_KM = 1000;
    private static final String POLYGON_ID = "profile";
    private static final String TEXT_GRID = "grid_label";
    private static final String TEXT_GRID_VERTICAL = "vertical";
    private static final String TEXT_GRID_HORIZONTAL = "horizontal";
    private static final String TEXT_POLICE = "Avenir";
    private static final int TEXT_SIZE = 10;


    /**
     * Constructor.
     *
     * @param elevationProfile    the elevation profile
     * @param highlightedPosition the highlighted position.
     */
    ElevationProfileManager(ReadOnlyObjectProperty<ElevationProfile> elevationProfile, ReadOnlyDoubleProperty highlightedPosition) {
        this.elevationProfile = elevationProfile;
        this.highlightedPosition = highlightedPosition;

        this.borderPane = new BorderPane();
        this.pane = new Pane();
        this.vbox = new VBox();

        this.screenToWorld = new SimpleObjectProperty<>(screenToWorld());
        this.worldToScreen = new SimpleObjectProperty<>(worldToScreen());
        this.highlightedLine = new Line();
        /*
        Creates a default size rectangle.
         */
        this.rectangle = new SimpleObjectProperty<>(DEFAULT_SIZE_RECTANGLE);
        this.grille = new Path();
        this.polygon = new Polygon();
        this.text = new Group();
        this.mousePosition = new SimpleDoubleProperty(Double.NaN);

        borderPane.getStylesheets().add(BORDER_PANE_STYLESHEET);
        vbox.setId(VBOX_ID);
        grille.setId(GRILLE_ID);
        polygon.setId(POLYGON_ID);
        pane.getChildren().addAll(grille, polygon, highlightedLine, text);
        borderPane.setCenter(pane);
        borderPane.setBottom(vbox);
        pane.setMinHeight(INSETS.getBottom() + INSETS.getTop() + 1);

        addBottomText();
        binders();
        setListeners();
        setEvents();
    }

    /**
     * Gives the pane with the profile of the itinerary and all the components that goes with it.
     *
     * @return the pane.
     */
    public Pane pane() {
        return borderPane;
    }

    /**
     * Computes the value of the highlighted position based on the position of the mouse on the graphic
     * representation of the profile.
     *
     * @return the property of the value of the highlighted position on the profile graphic representation.
     */
    public DoubleProperty mousePositionOnProfileProperty() {
        return mousePosition;
    }


    /**
     * Creates the grid of the profile.
     */
    private void createGrid() {
        grille.getElements().clear();
        text.getChildren().clear();

        Point2D startPoint;
        Point2D endPoint;

        double minElevation = elevationProfile.get().minElevation();
        double maxElevation = elevationProfile.get().maxElevation();
        double length = elevationProfile.get().length();
        String textValue;

        /*
         * Creates the vertical lines and adds the text associated to each line.
         */

        double distanceMade = 0;

        while (distanceMade <= length) {
            startPoint = worldToScreen.get().transform(distanceMade, minElevation);
            endPoint = worldToScreen.get().transform(distanceMade, maxElevation);
            textValue = "" + ((int) distanceMade / TO_KM);

            grille.getElements().add(new MoveTo(startPoint.getX(), startPoint.getY()));
            grille.getElements().add(new LineTo(endPoint.getX(), endPoint.getY()));
            addTextGrid(startPoint.getX(), startPoint.getY(), textValue, true);

            distanceMade += realStepX();
        }

        /*
         * Creates the horizontal lines and adds the text associated to each line.
         */

        addFirstHorizontalLine(grille, minElevation, length);

        distanceMade = realStepY() - elevationProfile.get().minElevation() % realStepY() + minElevation;
        while (distanceMade <= maxElevation) {

            textValue = "" + ((int) distanceMade);
            startPoint = worldToScreen.get().transform(0, distanceMade);
            endPoint = worldToScreen.get().transform(length, distanceMade);

            grille.getElements().add(new MoveTo(startPoint.getX(), startPoint.getY()));
            grille.getElements().add(new LineTo(endPoint.getX(), endPoint.getY()));
            addTextGrid(startPoint.getX(), startPoint.getY(), textValue, false);
            distanceMade += realStepY();
        }

    }


    /**
     * Finds the real step between each line X base on the minimal screen step allowed.
     *
     * @return the real step between each line X.
     */
    private int realStepX() {
        double numberOfLines = Math.floor(rectangle.get().getWidth() / MIN_SCREEN_STEP_X);
        for (int step : POS_STEPS) {
            if (numberOfLines * step >= elevationProfile.get().length()) {
                return step;
            }
        }
        return POS_STEPS[POS_STEPS.length - 1];
    }

    /**
     * Finds the real step between each line Y base on the minimal screen step allowed.
     *
     * @return the real step between each line Y.
     */
    private int realStepY() {
        double numberOfLines = Math.floor(rectangle.get().getHeight() / MIN_SCREEN_STEP_Y);
        for (int step : ELE_STEPS) {
            if (numberOfLines * step >= (elevationProfile.get().maxElevation() - elevationProfile.get().minElevation())) {
                return step;
            }
        }
        return ELE_STEPS[ELE_STEPS.length - 1];
    }

    /**
     * Adds the first line of the horizontal line in the grid.
     *
     * @param grid         the grid
     * @param minElevation the minimal elevation of the profile
     * @param length       the length of the profile
     */
    private void addFirstHorizontalLine(Path grid, double minElevation, double length) {
        Point2D startPoint = worldToScreen.get().transform(0, minElevation);
        Point2D endPoint = worldToScreen.get().transform(length, minElevation);

        grid.getElements().add(new MoveTo(startPoint.getX(), startPoint.getY()));
        grid.getElements().add(new LineTo(endPoint.getX(), endPoint.getY()));
    }


    /**
     * Creates the polygon.
     */
    private void createPolygon() {
        polygon.getPoints().clear();

        double length = elevationProfile.get().length();
        double minElevation = elevationProfile.get().minElevation();
        double distanceMade;
        Point2D point;


        /* First point of the polygon to start. */
        Point2D startPoint = worldToScreen.get().transform(0, minElevation);
        polygon.getPoints().addAll(startPoint.getX(), startPoint.getY());

        for (double i = rectangle.get().getMinX(); i <= rectangle.get().getMaxX(); i++) {
            /* Distance made in the real world equivalent to one pixel step on the screen */
            distanceMade = screenToWorld.get().transform(i, 0).getX();

            point = worldToScreen.get().transform(distanceMade, elevationProfile.get().elevationAt(distanceMade));
            polygon.getPoints().addAll(point.getX(), point.getY());
            i++;
        }

        /* Last two points of the polygon to complete it.*/
        point = worldToScreen.get().transform(length, elevationProfile.get().elevationAt(length));
        polygon.getPoints().addAll(point.getX(), point.getY(), point.getX(), startPoint.getY());
    }

    /**
     * Adds a text at a certain position used for the vertical lines.
     *
     * @param x          the X position on the pane
     * @param y          the Y position on the pane
     * @param value      the text
     * @param isVertical if the text added is on for the vertical lines or the horizontal lines
     */
    private void addTextGrid(double x, double y, String value, boolean isVertical) {
        Text textGrid = new Text(x, y, value);
        textGrid.getStyleClass().add(TEXT_GRID);
        textGrid.setFont(Font.font(TEXT_POLICE, TEXT_SIZE));
        double width;

        if (isVertical) {
            /* Text next to the vertical lines.*/
            textGrid.getStyleClass().add(TEXT_GRID_HORIZONTAL);
            textGrid.textOriginProperty().set(VPos.TOP);
            width = textGrid.prefWidth(0) / 2;
            textGrid.setX(x - width);
        } else {
            /* Text next to the horizontal lines. */
            textGrid.getStyleClass().add(TEXT_GRID_VERTICAL);
            textGrid.textOriginProperty().set(VPos.CENTER);
            width = textGrid.prefWidth(0);
            textGrid.setX(x - width - 2);
        }
        text.getChildren().add(textGrid);
    }

    /**
     * Transform screen (pane) coordinates to the real world coordinates.
     *
     * @return the transformation
     */
    private Transform screenToWorld() {
        Affine affine = new Affine();

        if (elevationProfile != null) {
            if (elevationProfile.get() != null) {
                double factorX = elevationProfile.get().length() / (pane.getWidth() - (INSETS.getLeft() + INSETS.getRight()));
                double factorY;
                /* Avoids the case when the "createInverse" (worldToScreen) is called, that there is a division
                by zero. This happens when the min elevation equals the max elevation on the profile.*/
                if (elevationProfile.get().maxElevation() - elevationProfile.get().minElevation() == 0) {
                    /* If so, the factorY equals to 1 (the neutral element of the multiplication). */
                    factorY = 1;
                } else {
                    factorY = ((elevationProfile.get().maxElevation() - elevationProfile.get().minElevation())
                            / (pane.getHeight() - (INSETS.getBottom() + INSETS.getTop())));
                }

                affine.prependTranslation(-INSETS.getLeft(), -pane.getHeight() + INSETS.getBottom());
                affine.prependScale(factorX, -factorY);
                affine.prependTranslation(0, elevationProfile.get().minElevation());
            }
        }
        return affine;
    }

    /**
     * Transform the coordinates of the real world to the ones on the screen (pane).
     *
     * @return the transformation
     */
    private Transform worldToScreen() {
        try {
            return screenToWorld().createInverse();
        } catch (NonInvertibleTransformException e) {
            /* If the reverse transformation can't be done, we just return
            a transformation that does nothing. Because the division by zero
            is being checked in the screenToWorld (in case the min elevation equals the max elevation),
            it should never happen. */

            return new Affine();
        }
    }

    /**
     * Adds the bottom text.
     */
    private void addBottomText() {
        if (elevationProfile.get() != null) {
            vbox.getChildren().clear();
            Text bottomText = new Text();
            vbox.getChildren().add(bottomText);

            ElevationProfile elevation = elevationProfile.get();

            bottomText.setText(String.format("Longueur : %.1f km" +
                            "     Montée : %.0f m" +
                            "     Descente : %.0f m" +
                            "     Altitude : de %.0f m à %.0f m", elevation.length() / TO_KM, elevation.totalAscent(), elevation.totalDescent(),
                    elevation.minElevation(), elevation.maxElevation()));
        }

    }

    /**
     * Binds all the properties to their respective object.
     */
    private void binders() {
        if (elevationProfile.get() != null) {

            /* The bindings for the transformation. */
            screenToWorld.bind(Bindings.createObjectBinding(this::screenToWorld,
                    pane.widthProperty(), pane.heightProperty(), elevationProfile));

            worldToScreen.bind(Bindings.createObjectBinding(this::worldToScreen,
                    pane.widthProperty(), pane.heightProperty(), elevationProfile));

            /* The bindings for the rectangle. */
            rectangle.bind(Bindings.createObjectBinding(() -> new Rectangle2D(
                            pane.layoutXProperty().get() + INSETS.getLeft(),
                            pane.layoutYProperty().get() + INSETS.getTop(),
                            Math.abs(pane.getWidth() - INSETS.getLeft() - INSETS.getRight()),
                            Math.abs(pane.getHeight() - INSETS.getTop() - INSETS.getBottom())),
                    pane.widthProperty(), pane.heightProperty(), pane.layoutXProperty(), pane.layoutYProperty()));

            /* The bindings for the highlighted line. */
            BooleanBinding visibleBinding = highlightedPosition.greaterThanOrEqualTo(0);
            highlightedLine.layoutXProperty().bind(Bindings.createDoubleBinding(() ->
                            worldToScreen.get().transform(highlightedPosition.get(), 0).getX(),
                    highlightedPosition, worldToScreen));
            highlightedLine.startYProperty().bind(Bindings.select(rectangle, "minY"));
            highlightedLine.endYProperty().bind(Bindings.select(rectangle, "maxY"));
            highlightedLine.visibleProperty().bind(visibleBinding);


        }
    }

    /**
     * Sets the listener of the rectangle and the listener of the elevationProfile.
     * If the rectangle changes, the grid and the polygon are recreated.
     * If the elevation profile changes everything is updated and recreated.
     */
    private void setListeners() {
        /*
        Listeners for the rectangle. If the rectangle changes,
        it recreates the grid and the polygon.
         */
        rectangle.addListener((observable, oldValue, newValue) -> {
            if (elevationProfile.get() != null) {
                createGrid();
                createPolygon();
            }
        });

        /*
        Listeners for the elevation profile. If it changes it updates the transformations
        and recreates the grid, the polygon the bottom text and the binders.
         */
        elevationProfile.addListener((o, oV, nV) -> {
            if (elevationProfile.get() != null) {
                createGrid();
                createPolygon();
                addBottomText();

                /* We reinitialize the binders, because if the elevationProfile
                was previously null the binders weren't set. */
                binders();
            }
        });
    }

    /**
     * Sets the event if the mouse is moved on the pane. It changes the value of the mousePosition.
     */
    private void setEvents() {
        pane.setOnMouseMoved(mouseMoved -> {
            double x = mouseMoved.getX();
            double y = mouseMoved.getY();

            if (rectangle.get().contains(x, y)) {
                mousePosition.set(screenToWorld.get().transform(x, y).getX());
            } else {
                mousePosition.set(Double.NaN);
            }
        });
        pane.setOnMouseExited(mouseExited -> mousePosition.set(Double.NaN));
    }


}
