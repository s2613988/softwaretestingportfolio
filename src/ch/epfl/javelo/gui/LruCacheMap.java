package ch.epfl.javelo.gui;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Implementation of an LRU cache using LinkedHashMap as suggested in the documentation.
 *
 * @param <K> Key
 * @param <E> Entry
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public class LruCacheMap<K, E> extends LinkedHashMap<K, E> {

    int maxEntries;

    /**
     * Constructor of LruCacheMap.
     *
     * @param maxEntries Maximum size of the cache memory
     */
    public LruCacheMap(int maxEntries) {
        super(0, 0.75f, true);
        this.maxEntries = maxEntries;
    }

    /**
     * Override as suggested by the documentation in order to remove
     * the least recently accessed entry when the cache is full.
     * https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/LinkedHashMap.html#removeEldestEntry(java.util.Map.Entry)
     *
     * @param eldest The least recently inserted entry in the map, or if
     *               this is an access-ordered map, the least recently accessed
     *               entry.  This is the entry that will be removed it this
     *               method returns {@code true}.  If the map was empty prior
     *               to the {@code put} or {@code putAll} invocation resulting
     *               in this invocation, this will be the entry that was just
     *               inserted; in other words, if the map contains a single
     *               entry, the eldest entry is also the newest.
     * @return true if the eldest entry should be removed from the map; false if it should be retained.
     */
    @Override
    protected boolean removeEldestEntry(Map.Entry eldest) {
        return size() > maxEntries;
    }

}
