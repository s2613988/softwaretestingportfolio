package ch.epfl.javelo.gui;

import ch.epfl.javelo.projection.PointWebMercator;
import javafx.geometry.Point2D;

/**
 * Record the represents the parameters of the background map in the graphic interface.
 *
 * @param zoomLevel the level of the zoom
 * @param x         the X coordinates of the top left corner point in the WebMercator system of coordinates
 * @param y         the Y coordinates of the top left corner point in the WebMercator system of coordinates
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public record MapViewParameters(int zoomLevel, double x, double y) {

    /**
     * Gives the Point2D representing the top-left corner point
     *
     * @return the Point2D representing the top-left corner point
     */
    public Point2D topLeft() {
        return new Point2D(x, y);
    }

    /**
     * Gives the same MapViewParameters as @this except that the
     * coordinates of the top-left corner point are the given x and y.
     *
     * @param x the X coordinates of the top left corner point in the WebMercator system of coordinates
     * @param y the Y coordinates of the top left corner point in the WebMercator system of coordinates
     * @return the same MapViewParameters as @this except that the
     * coordinates of the top-left corner point are the given x and y.
     */
    public MapViewParameters withMinXY(double x, double y) {
        return new MapViewParameters(zoomLevel, x, y);
    }

    /**
     * Gives the corresponding WebMercator point to the coordinates
     * of a point relative to the top-corner point.
     *
     * @param xRelative the X coordinates relative to the top-corner point.
     * @param yRelative the Y coordinates relative to the top-corner point.
     * @return the corresponding WebMercator point.
     */
    public PointWebMercator pointAt(double xRelative, double yRelative) {
        return PointWebMercator.of(zoomLevel, this.x + xRelative, this.y + yRelative);
    }

    /**
     * Gives the relative X coordinates of a WebMercator point
     * to the top-left corner point of a MapViewParameter
     *
     * @param pointWebMercator the WebMercator point
     * @return the relative X coordinate
     */
    public double viewX(PointWebMercator pointWebMercator) {
        return pointWebMercator.xAtZoomLevel(zoomLevel) - this.x;
    }

    /**
     * Gives the relative Y coordinates of a WebMercator point
     * to the top-left corner point of a MapViewParameter
     *
     * @param pointWebMercator the WebMercator point
     * @return the relative Y coordinate
     */
    public double viewY(PointWebMercator pointWebMercator) {
        return pointWebMercator.yAtZoomLevel(zoomLevel) - this.y;
    }
}
