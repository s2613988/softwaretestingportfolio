package ch.epfl.javelo.gui;

import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.routing.ElevationProfile;
import ch.epfl.javelo.routing.Route;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Class that allows to transform an itinerary in its GPX format and to create a file out of it.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public class GpxGenerator {

    private static final String GPX_URI_ADDRESS = "http://www.topografix.com/GPX/1/1";
    private static final String GPX_ATTRIBUTE_URI_ADDRESS = "http://www.w3.org/2001/XMLSchema-instance";
    private static final String GPX_ATTRIBUTE_QUALIFIED_NAME = "xsi:schemaLocation";
    private static final String GPX_ATTRIBUTE_SOURCES1 = "http://www.topografix.com/GPX/1/1 ";
    private static final String GPX_ATTRIBUTE_SOURCES2 = "http://www.topografix.com/GPX/1/1/gpx.xsd";
    private static final String PROJECT_NAME = "JaVelo";
    private static final String ROUTE_NAME = "Route JaVelo";

    /**
     * Private constructor to prevent instantiation.
     */
    private GpxGenerator() {
    }

    /**
     * Creates a GPX document out of an itinerary and its profile.
     *
     * @param route            the itinerary
     * @param elevationProfile the profile associated to the route (avoids having Double.NaN values)
     * @return a GPX document
     */
    public static Document createGpx(Route route, ElevationProfile elevationProfile) {
        return fillDoc(route, elevationProfile);
    }

    /**
     * Writes the corresponding document GPX into a given file.
     *
     * @param path             the path name of the file
     * @param route            the itinerary
     * @param elevationProfile the profile of the itinerary
     * @throws IOException if the file name is not found
     */
    public static void writeGpx(String path, Route route, ElevationProfile elevationProfile) throws IOException {
        Document doc = createGpx(route, elevationProfile);
        try (Writer w = new FileWriter(path)) {

            Transformer transformer = TransformerFactory
                    .newDefaultInstance()
                    .newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc),
                    new StreamResult(w));
        } catch (TransformerException e) {
            throw new Error(e);
        }
    }

    /**
     * Creates a new document.
     *
     * @return a new document
     */
    private static Document newDocument() {
        try {
            return DocumentBuilderFactory
                    .newDefaultInstance()
                    .newDocumentBuilder()
                    .newDocument();
        } catch (ParserConfigurationException e) {
            throw new Error(e);
        }
    }

    /**
     * Fills the document with the information of the route and its profile.
     *
     * @param route            the itinerary
     * @param elevationProfile the profile of the itinerary
     * @return the document once filled
     */
    private static Document fillDoc(Route route, ElevationProfile elevationProfile) {

        /* Creates the tree of elements of the gpx. */
        Document doc = newDocument();

        Element root = doc
                .createElementNS(GPX_URI_ADDRESS,
                        "gpx");
        doc.appendChild(root);

        root.setAttributeNS(
                GPX_ATTRIBUTE_URI_ADDRESS,
                GPX_ATTRIBUTE_QUALIFIED_NAME,
                GPX_ATTRIBUTE_SOURCES1
                        + GPX_ATTRIBUTE_SOURCES2);
        root.setAttribute("version", "1.1");
        root.setAttribute("creator", PROJECT_NAME);

        Element metadata = doc.createElement("metadata");
        root.appendChild(metadata);

        Element name = doc.createElement("name");
        metadata.appendChild(name);
        name.setTextContent(ROUTE_NAME);

        Element rte = doc.createElement("rte");
        root.appendChild(rte);

        /* Fills the document with the information of the route and its profile.*/

        double distanceMade = 0;
        int counter = 0;

        for (PointCh point : route.points()) {
            Element retpt = doc.createElement("rtept");
            retpt.setAttribute("lat", Double.toString(Math.toDegrees(point.lat())));
            retpt.setAttribute("lon", Double.toString(Math.toDegrees(point.lon())));

            Element ele = doc.createElement("ele");
            ele.setTextContent(Double.toString(elevationProfile.elevationAt(distanceMade)));

            retpt.appendChild(ele);
            rte.appendChild(retpt);

            if (counter != route.points().size() - 1) {
                distanceMade += route.edges().get(counter).length();
            }

            counter++;
        }

        return doc;
    }
}
