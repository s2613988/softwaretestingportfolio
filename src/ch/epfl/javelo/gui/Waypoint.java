package ch.epfl.javelo.gui;

import ch.epfl.javelo.projection.PointCh;

/**
 * Represents a waypoint of an itinerary.
 *
 * @param wayPointCh the waypoint in swiss coordinates (PointCh)
 * @param closestNodeId the ID of the closest node
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public record Waypoint(PointCh wayPointCh, int closestNodeId) {
}
