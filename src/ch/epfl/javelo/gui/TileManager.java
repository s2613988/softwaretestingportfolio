package ch.epfl.javelo.gui;

import ch.epfl.javelo.Preconditions;

import javafx.scene.image.Image;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

/**
 * Manager for Tiles
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class TileManager {

    private final Path path;
    private final URI serverName;
    private final Map<TileId, Image> cache;

    private final static int CACHE_SIZE = 100;

    /**
     * Constructor of TileManager.
     *
     * @param path       File-path to the desired cache-disk
     * @param serverName Server URL to download images
     */
    public TileManager(Path path, String serverName) {
        this.path = path;
        this.serverName = URI.create("https://" + serverName);
        cache = new LruCacheMap<>(CACHE_SIZE);
    }

    /**
     * Gives the image corresponding to the tileId.
     *
     * @param tileId ID of the tile
     * @return Image of the tile
     */
    public Image imageForTileAt(TileId tileId) {
        Preconditions.checkArgument(TileId.isValid(tileId.zoom, tileId.x, tileId.y));

        if (!cache.containsKey(tileId)) {
            if (!loadFromDisk(tileId)) {
                downloadFromServer(tileId);
                loadFromDisk(tileId);
            }
        }
        return cache.get(tileId);
    }

    /**
     * Loads a tile's image from Disk-cache to Memory-cache.
     *
     * @param tileId ID of the tiles
     * @return true if the loading is successful
     */
    private boolean loadFromDisk(TileId tileId) {
        try (InputStream i = new FileInputStream(path.resolve(imagePath(tileId)).toString())) {
            cache.put(tileId, new Image(i));
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Downloads a tile's image from the server to Disk-cache.
     *
     * @param tileId ID of the tiles
     * @throws RuntimeException Directories creation error / image downloading error
     */
    private void downloadFromServer(TileId tileId) {
        if (!Files.exists(path.resolve(imagePath(tileId)).getParent())) {
            try {
                Files.createDirectories(path.resolve(imagePath(tileId)).getParent());
            } catch (IOException e) {
                throw new RuntimeException("TileCache unable to create directories");
            }
        }

        try {
            URL url = serverName.resolve("/" + imagePath(tileId)).toURL();
            URLConnection c = url.openConnection();
            c.setRequestProperty("User-Agent", "JaVelo");
            try (InputStream in = c.getInputStream();
                 OutputStream out = new FileOutputStream(path.resolve(imagePath(tileId)).toString())) {
                in.transferTo(out);
            }
        } catch (IOException e) {
            throw new RuntimeException("TileCache unable to download tiles");
        }
    }

    /**
     * Path of a tile's image.
     *
     * @param tileId ID of the tile
     * @return path of the image
     */
    private String imagePath(TileId tileId) {
        return tileId.zoom + "/" + tileId.x + "/" + tileId.y + ".png";
    }

    /**
     * ID Of a Tile
     */
    public record TileId(int zoom, int x, int y) {

        /**
         * Check the validity of An ID.
         *
         * @param zoom zoom level
         * @param x    x coordinate of the tile
         * @param y    y coordinate of the tile
         * @return true if the TileID is valid
         */
        public static boolean isValid(int zoom, int x, int y) {
            Preconditions.checkArgument(x >= 0 && y >= 0 && zoom >= 0);
            double maxCoord = Math.pow(4, zoom - 1) - 1;
            return x <= maxCoord && y <= maxCoord;
        }
    }
}
