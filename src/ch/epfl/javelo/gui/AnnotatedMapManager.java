package ch.epfl.javelo.gui;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.PointWebMercator;
import ch.epfl.javelo.routing.Route;
import ch.epfl.javelo.routing.RoutePoint;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Point2D;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.util.function.Consumer;

/**
 * Manager of the map / routes / waypoints
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class AnnotatedMapManager {

    private final StackPane mainPane;
    private final ObjectProperty<Point2D> mousePosition;
    private final DoubleProperty mousePositionOnRoute;

    private static final double MAX_DISTANCE = 15;
    private static final int ZOOM_LEVEL = 12;
    private static final double INITIAL_X = 543200;
    private static final double INITIAL_Y = 370650;

    /**
     * Constructor of AnnotatedMapManager
     *
     * @param graph         graph of the manager
     * @param tileManager   tileManager for the map
     * @param routeBean     routeBean for the routeManager
     * @param errorConsumer errorConsumer
     */
    AnnotatedMapManager(Graph graph, TileManager tileManager, RouteBean routeBean, Consumer<String> errorConsumer) {

        MapViewParameters mapViewParameters =
                new MapViewParameters(ZOOM_LEVEL, INITIAL_X, INITIAL_Y);
        ObjectProperty<MapViewParameters> mapViewParametersP =
                new SimpleObjectProperty<>(mapViewParameters);

        mousePositionOnRoute = new SimpleDoubleProperty();
        mousePosition = new SimpleObjectProperty<>(new Point2D(0, 0));

        //Pane setup
        WaypointsManager waypointsManager = new WaypointsManager(graph, mapViewParametersP, routeBean.waypoints(), errorConsumer);

        BaseMapManager baseMapManager = new BaseMapManager(tileManager, waypointsManager, mapViewParametersP);

        RouteManager routeManager = new RouteManager(routeBean, mapViewParametersP);

        this.mainPane = new StackPane(baseMapManager.pane(),
                routeManager.pane(), waypointsManager.pane());

        //MousePosition set
        mainPane.setOnMouseMoved(event -> mousePosition.set(new Point2D(event.getX(), event.getY())));

        mainPane.setOnMouseExited(event -> mousePosition.set(new Point2D(Double.NaN, Double.NaN)));

        //MousePositionOnRoute set
        mousePosition.addListener((o, oV, nV) -> {
            if (!Double.isNaN(nV.getX())) {
                Route route = routeBean.routeProperty().get();
                if (route != null) {
                    PointCh pointCh = mapViewParametersP.getValue().pointAt(nV.getX(), nV.getY()).toPointCh();
                    if (pointCh != null) {
                        RoutePoint closestPointOnRoute = route.pointClosestTo(pointCh);
                        double distance = Math2.norm(mapViewParametersP.getValue().viewX(PointWebMercator.ofPointCh(closestPointOnRoute.point())) - nV.getX(),
                                mapViewParametersP.getValue().viewY(PointWebMercator.ofPointCh(closestPointOnRoute.point())) - nV.getY());

                        if (distance <= MAX_DISTANCE) {
                            mousePositionOnRoute.setValue(closestPointOnRoute.position());
                        } else {
                            mousePositionOnRoute.setValue(Double.NaN);
                        }
                    }else {
                        mousePositionOnRoute.setValue(Double.NaN);
                    }
                } else {
                    mousePositionOnRoute.setValue(Double.NaN);
                }
            } else {
                mousePositionOnRoute.setValue(Double.NaN);
            }
        });
    }


    /**
     * Pane of the manager.
     *
     * @return pane
     */
    public Pane pane() {
        return mainPane;
    }

    /**
     * MousePositionOnRouteProperty's getter.
     *
     * @return property of mousePositionOnRoute
     */
    public DoubleProperty mousePositionOnRouteProperty() {
        return mousePositionOnRoute;
    }
}
