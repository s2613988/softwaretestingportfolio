package ch.epfl.javelo.gui;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.routing.CityBikeCF;
import ch.epfl.javelo.routing.CostFunction;
import ch.epfl.javelo.routing.RouteComputer;
import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Dimension2D;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.function.Consumer;

/**
 * Main class of Javelo
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class JaVelo extends Application {

    private static final String GRAPH_DATA_PATH = "osm-data";
    private static final String CACHE_PATH = "osm-cache";
    private static final String TILE_SERVER_NAME = "tile.openstreetmap.org";
    private static final String GPX_PATH = "javelo.gpx";


    private static final String CSS_STYLESHEET = "map.css";
    private static final String WINDOW_NAME = "Javelo";
    private static final Dimension2D WINDOW_DIMENSIONS = new Dimension2D(800, 600);

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Graph graph = Graph.loadFrom(Path.of(GRAPH_DATA_PATH));

        TileManager tileManager = new TileManager(Path.of(CACHE_PATH), TILE_SERVER_NAME);

        CostFunction costFunction = new CityBikeCF(graph);
        RouteComputer routeComputer = new RouteComputer(graph, costFunction);
        RouteBean routeBean = new RouteBean(routeComputer);

        ErrorManager errorManager = new ErrorManager();
        Consumer<String> errorConsumer = errorManager::displayError;

        //Split pane between AnnotatedMap and ElevationProfile
        AnnotatedMapManager annotatedMapManager = new AnnotatedMapManager(graph, tileManager, routeBean, errorConsumer);

        ElevationProfileManager elevationProfileManager = new ElevationProfileManager(routeBean.elevationProfileProperty(), routeBean.highlightedPositionProperty());
        SplitPane.setResizableWithParent(elevationProfileManager.pane(), false);

        SplitPane centerContent = new SplitPane(annotatedMapManager.pane());

        routeBean.routeProperty().addListener((o, oV, nV) -> {
            if (nV == null) {
                centerContent.getItems().remove(elevationProfileManager.pane());
            } else {
                if (!centerContent.getItems().contains(elevationProfileManager.pane())) {
                    centerContent.getItems().add(elevationProfileManager.pane());
                }
            }
        });

        centerContent.setOrientation(Orientation.VERTICAL);

        //HighlightedPosition bind
        annotatedMapManager.mousePositionOnRouteProperty().addListener((o, oV, nV) -> {
            if (nV.doubleValue() >= 0 && !Double.isNaN(nV.doubleValue())) {
                routeBean.highlightedPositionProperty().bind(annotatedMapManager.mousePositionOnRouteProperty());
            } else {
                routeBean.highlightedPositionProperty().bind(elevationProfileManager.mousePositionOnProfileProperty());
            }
        });

        //Menu
        MenuItem exportGpx = new MenuItem("Exporter GPX");

        BooleanProperty routeNull = new SimpleBooleanProperty(routeBean.routeProperty().getValue() == null);
        routeBean.routeProperty().addListener((o, oV, nV) -> routeNull.setValue(nV == null));

        exportGpx.disableProperty().bind(routeNull);

        exportGpx.setOnAction(event -> {
            if (routeBean.routeProperty().get() != null) {
                try {
                    GpxGenerator.writeGpx(GPX_PATH, routeBean.routeProperty().get(), routeBean.elevationProfileProperty().getValue());
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            }
        });

        Menu fileMenu = new Menu("Fichier", exportGpx.getGraphic(), exportGpx);
        MenuBar menuBar = new MenuBar(fileMenu);

        //Main pane setup
        StackPane content = new StackPane(centerContent, errorManager.pane());
        BorderPane mainPane = new BorderPane(content, menuBar, null, null, null);
        mainPane.getStylesheets().add(CSS_STYLESHEET);

        //Stage setup
        primaryStage.setMinWidth(WINDOW_DIMENSIONS.getWidth());
        primaryStage.setMinHeight(WINDOW_DIMENSIONS.getHeight());
        primaryStage.setScene(new  Scene(mainPane));
        primaryStage.setTitle(WINDOW_NAME);

        primaryStage.show();

    }
}
