package ch.epfl.javelo;

/**
 * Class which can compute a certain amount of mathematical
 * computations on vectors and more.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class Math2 {
    /**
     * Private constructor to prevent instantiation.
     */
    private Math2() {
    }

    /**
     * Computes the integer part by excess of the division of x/y.
     *
     * @param x value x (strictly positive)
     * @param y value y (positive)
     * @return the floor part of the division of x with y
     * @throws IllegalArgumentException if x is and y are not
     */
    public static int ceilDiv(int x, int y) {
        Preconditions.checkArgument(x >= 0 && y > 0);
        return (x + y - 1) / y;
    }

    /**
     * Gives the coordinate y of a given x on a line going from (0,y0) to (1,y1).
     *
     * @param y0 coordinates Y of the line for the value x = 0
     * @param y1 coordinates Y of the line for the value x = 1
     * @param x  the given value x
     * @return the coordinates of the point y formed by the line of (0,y0) and (1,y1)
     */
    public static double interpolate(double y0, double y1, double x) {
        return Math.fma((y1 - y0), x, y0);
    }

    /**
     * Gives the value v if it's between min and max, or it gives max/min.
     *
     * @param min the minimal value
     * @param v   the given value
     * @param max the maximal value
     * @return v if it's between min and max, max if it is bigger than
     * max and min if it is smaller than min
     * @throws IllegalArgumentException if the preconditions are not respected
     */
    public static int clamp(int min, int v, int max) {
        Preconditions.checkArgument(min <= max);
        return Math.max(Math.min(v, max), min);
    }

    /**
     * Overloading of clamp with doubles.
     *
     * @param min the minimal value
     * @param v   the given value
     * @param max the maximal value
     * @return v if it's between min and max, max if it is bigger than
     * max and min if it is smaller than min
     * @throws IllegalArgumentException if the minimal value (min)
     * is not strictly inferior to the maximal value (max)
     */
    public static double clamp(double min, double v, double max) {
        Preconditions.checkArgument(min <= max);
        return Math.max(Math.min(v, max), min);
    }

    /**
     * Computes for a given x the value of the reciprocal function
     * of the hyperbolic sinus.
     *
     * @param x the given x
     * @return the value of the reciprocal function of the hyperbolic
     * sinus with as input x
     */
    public static double asinh(double x) {
        return Math.log(x + Math.sqrt(1 + x * x));
    }

    /**
     * Computes the scalar product between two vectors.
     *
     * @param uX coordinates X of the first vector
     * @param uY coordinates Y of the first vector
     * @param vX coordinates X of the second vector
     * @param vY coordinates Y of the second vector
     * @return the scalar product between two vectors
     */
    public static double dotProduct(double uX, double uY, double vX, double vY) {
        return Math.fma(uX, vX, uY * vY);
    }

    /**
     * Square of the norm of a vector.
     *
     * @param uX coordinates X of the vector
     * @param uY coordinates Y of the vector
     * @return the square of the norm of a vector
     */
    public static double squaredNorm(double uX, double uY) {
        return dotProduct(uX, uY, uX,uY);
    }

    /**
     * Gives the norm of a vector.
     *
     * @param uX coordinates X of the vector
     * @param uY coordinates Y of the vector
     * @return the norm of the vector
     */
    public static double norm(double uX, double uY) {
        return Math.sqrt(squaredNorm(uX, uY));
    }

    /**
     * Gives the projection of on vector on the other (going from the same point).
     *
     * @param aX coordinates X of the point A
     * @param aY coordinates Y of the point A
     * @param bX coordinates X of the point B
     * @param bY coordinates Y of the point B
     * @param pX coordinates X of the point P
     * @param pY coordinates Y of the point P
     * @return the length of the projection of the vector going from
     * the coordinates A(aX,aY) to P(pX,py) on the vector going from
     * the coordinates A(aX,aY) to B(bX,bY)
     */
    public static double projectionLength(double aX, double aY, double bX, double bY, double pX, double pY) {
        double uX = pX - aX;
        double uY = pY - aY;
        double vX = bX - aX;
        double vY = bY - aY;
        return dotProduct(uX, uY, vX, vY) / norm(vX, vY);
    }
}
