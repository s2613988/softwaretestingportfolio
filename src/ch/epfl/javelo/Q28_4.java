package ch.epfl.javelo;

/**
 * Class that converts a decimal number in the Q28_4 format and vice versa.
 *
 * @author Julien Ray (328357)
 * @author Yannik Krone (347243)
 */
public final class Q28_4 {
    public final static int DEFAULT_BITS_LEFT = 4;

    /**
     * Private constructor to prevent instantiation.
     */
    private Q28_4() {
    }

    /**
     * Computes the corresponding value of an int in a Q28.4 value.
     *
     * @param i a given int
     * @return the corresponding value of i(int) in Q28.4 value
     */
    public static int ofInt(int i) {
        return i << DEFAULT_BITS_LEFT;
    }

    /**
     * Computes the value equal to the given Q28.4 value.
     *
     * @param q28_4 a given value in the Q28_4 format
     * @return the value equal to the given Q28.4 value
     */
    public static double asDouble(int q28_4) {
        return Math.scalb((double) q28_4, -DEFAULT_BITS_LEFT);
    }

    /**
     * Computes the value equal to the given Q28.4 value.
     *
     * @param q28_4 a given value in the Q28_4 format
     * @return the value equal to the given Q28.4 value
     */
    public static float asFloat(int q28_4) {
        return Math.scalb(q28_4, -DEFAULT_BITS_LEFT);
    }
}

