package ch.epfl.javelo.routing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import ch.epfl.javelo.projection.PointCh;

public class MultiRouteTest {
    /**
     * _____PREPARATION_____
     */
    
    /**
     * Desired edge shape _/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_
     * This allows testing on a continuous route with a known shape that is not a straight line
     * Bellow are the paramters used to generate the route
     */
    private static final int ORIGIN_N = 1_200_000;
    private static final int ORIGIN_E = 2_600_000;
    private static final double SPIKE_E_LENGTH = 1025.0;
    private static final double SPIKE_N_ELEVATION = 64.0;
    private static final double TRIANGLE_SLOPE = SPIKE_N_ELEVATION / SPIKE_E_LENGTH;


    // Sample list of edges generated according to the desired shape
    private static List<Edge> sampleEdges(int n_edges) {
        List<Edge> edges = new ArrayList<>();
        for (int i = 0; i < n_edges; i++) {
            if (i%3 == 0){
                edges.add(new Edge(i, i + 1, pathPoint(i), pathPoint(i + 1), SPIKE_E_LENGTH, x -> 0));
            }
            else {
                PointCh from = pathPoint(i);
                PointCh to = pathPoint(i + 1);
                double slope = (i + 1)%3 == 2 ? TRIANGLE_SLOPE : -TRIANGLE_SLOPE;
                double start = (i + 1)%3 == 2 ? 0 : SPIKE_N_ELEVATION;
                edges.add(new Edge(i, i + 1, from, to,SPIKE_E_LENGTH, x -> slope * x + start));
            }
        }
        return Collections.unmodifiableList(edges);
    }

    // Function that returns the point of our desired shape at a given position
    private static PointCh pathPoint(int i) {
        return new PointCh(
            ORIGIN_E + i * SPIKE_E_LENGTH, 
            ORIGIN_N + ((i % 3 == 2) ? SPIKE_N_ELEVATION: 0.0));
    }

    /**
     * _____TESTS_____
     */
    // Test of length
    @Test
    public void sampleRoutelength() {
        List<Route> routes = new ArrayList<>();
        int n_edges = 10;
        for (Edge edge : sampleEdges(n_edges)) {
            routes.add(new SingleRoute(List.of(edge)));
        }
        MultiRoute route = new MultiRoute(routes);
        assertEquals(n_edges * SPIKE_E_LENGTH, route.length(), 0.0);
    }

    // Test of points
    @Test
    public void sampleRoutePoints() {
        List<Route> routes = new ArrayList<>();
        int n_edges = 10;
        for (Edge edge : sampleEdges(n_edges)) {
            routes.add(new SingleRoute(List.of(edge)));
        }
        MultiRoute route = new MultiRoute(routes);
        assertEquals(n_edges + 1,route.points().size());
        assertEquals(pathPoint(0), route.points().get(0));
        for (int j = 0; j < n_edges; j++){
            assertEquals(pathPoint(j), route.points().get(j));
        }
    }

    // Test pointAt
    @Test
    public void sampleRoutepointAt() {
        List<Route> routes = new ArrayList<>();
        int n_edges = 10;
        for (Edge edge : sampleEdges(n_edges)) {
            routes.add(new SingleRoute(List.of(edge)));
        }
        MultiRoute route = new MultiRoute(routes);

        // Points outside the route
        assertEquals(pathPoint(0), route.pointAt(-1));
        assertEquals(pathPoint(n_edges), route.pointAt(1e10));

        // Edges points
        for (int i = 0; i < n_edges; i++) {
            assertEquals(pathPoint(i), route.pointAt(i * SPIKE_E_LENGTH));
        }
    }

    // Test elevationAt
    @Test
    public void sampleRouteElevationAt() {
        List<Route> routes = new ArrayList<>();
        int n_edges = 10;
        for (Edge edge : sampleEdges(n_edges)) {
            routes.add(new SingleRoute(List.of(edge)));
        }
        MultiRoute route = new MultiRoute(routes);

        // Points outside the route
        assertEquals(0.0, route.elevationAt(-1));

        // Edges points
        for (int i = 0; i < n_edges; i+=1) {
            if (i%3 == 2){
                assertEquals(SPIKE_N_ELEVATION, route.elevationAt(i * SPIKE_E_LENGTH), 0.0);
            }
            else {
                assertEquals(0, route.elevationAt(i * SPIKE_E_LENGTH), 0.0);
            }
        }
    }

    // Test nodeClosestTo
    @Test
    public void sampleRouteNodeClosestTo(){
        int n_edges = 10;
        List<Route> routes = new ArrayList<>();
        for (Edge edge : sampleEdges(n_edges)) {
            routes.add(new SingleRoute(List.of(edge)));
        }
        MultiRoute route = new MultiRoute(routes);
        for (int i = 0; i < n_edges; i++) {
            for (double j=-0.25; j <=0.25; j+=0.25){
                assertEquals(i, route.nodeClosestTo((i + j) * SPIKE_E_LENGTH));
            }
        }
    }

    // Test PointClosestTo
    @Test
    public void sampleRoutePointClosestTo(){
        int n_edges = 10;
        List<Route> routes = new ArrayList<>();
        for (Edge edge : sampleEdges(n_edges)) {
            routes.add(new SingleRoute(List.of(edge)));
        }
        MultiRoute route = new MultiRoute(routes);
        for (int i = 0; i < n_edges; i++) {
            switch (i%3) {
                // Above the spike
                case 2:
                    PointCh refPoint = pathPoint(i);
                    PointCh AboveRefPoint = new PointCh(refPoint.e(), refPoint.n() + 100);
                    RoutePoint resultPoint = route.pointClosestTo(AboveRefPoint);
                    assertEquals(refPoint, resultPoint.point());
                    assertEquals(i * SPIKE_E_LENGTH, resultPoint.position(), 0.0);
                    assertEquals(100, resultPoint.distanceToReference(), 0.0);
                    break;

                // Below the spike
                default:
                    refPoint = pathPoint(i);
                    PointCh BelowRefPoint = new PointCh(refPoint.e(), refPoint.n() - 100);
                    resultPoint = route.pointClosestTo(BelowRefPoint);
                    assertEquals(refPoint, resultPoint.point());
                    assertEquals(i * SPIKE_E_LENGTH, resultPoint.position(), 0.0);
                    assertEquals(100, resultPoint.distanceToReference(), 0.0);
                    break;
            }
        }
    }
}
