package ch.epfl.javelo.routing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;

import org.junit.Test;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;

public class RouteComputerTest {
    // Random parameters
    public final static int RANDOM_SEED = 1957;
    public final static int RANDOM_ITERATIONS = 1000;


    /**
     * ____PREPARATION____
     */
    private static Graph graph;

    public static RouteComputer sampleRouteComputer() {
        try{
            graph = Graph.loadFrom(Path.of("osm-data"));
            return new RouteComputer(graph, new CityBikeCF(graph));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * ____TESTS____
     */
    // Returns null for impossible route
    @Test
    public void routeComputerReturnsNullForImpossibleRoute() {
        RouteComputer rc = sampleRouteComputer();
        assertNull(rc.bestRouteBetween(149195, 153181));
    }

    // Correct example
    @Test
    public void routeComputerComputesCorrectRouteForGivenExample() {
        RouteComputer rc = sampleRouteComputer();
        Route route = rc.bestRouteBetween(16040, 115958);
        assertNotNull(route);
        assertEquals(7859.9375, route.length(), 1);
        assertEquals(464, route.points().size());
        assertEquals(463, route.edges().size());
        assertEquals(2535770, route.pointAt(10).e(), 1);
        assertEquals(1158090, route.pointAt(10).n(), 1);
        assertEquals(2535859, route.pointAt(100).e(), 1);
        assertEquals(1158092, route.pointAt(100).n(), 1);
        
    }

    // Doesnt throws on Random valid instances
    @Test
    public void routeComputerDoesntThrowOnRandomValidInstances() {
        RouteComputer rc = sampleRouteComputer();
        List<Integer> nodes = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                PointCh point = new PointCh(SwissBounds.MIN_E + i * (SwissBounds.MAX_E - SwissBounds.MIN_E) / 100,
                                            SwissBounds.MIN_N + j * (SwissBounds.MAX_N - SwissBounds.MIN_N) / 100);
                int node = graph.nodeClosestTo(point, 100);
                if (node != -1) {
                    nodes.add(node);
                }
            }
        }

        //sample randomly a pair of nodes
        RandomGenerator rng = RandomGeneratorFactory.getDefault().create(RANDOM_SEED);
        for (int i = 0; i < RANDOM_ITERATIONS; i++) {
            int node1 = nodes.get(rng.nextInt(nodes.size()));
            int node2 = nodes.get(rng.nextInt(nodes.size()));

            if (node1 == node2) {
                continue;
            }
            rc.bestRouteBetween(node1, node2);
        }
    }

        
}
