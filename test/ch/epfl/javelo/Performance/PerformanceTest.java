package ch.epfl.javelo.Performance;


import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.SwissBounds;
import ch.epfl.javelo.routing.CityBikeCF;
import ch.epfl.javelo.routing.CostFunction;
import ch.epfl.javelo.routing.RouteComputer;
import ch.epfl.javelo.projection.PointCh;

public class PerformanceTest {
    // Random parameters
    public final static int RANDOM_SEED = 1957;
    public final static int RANDOM_ITERATIONS = 1000;
    public static void main(String[] args) {
        try {
            routComputerTiming();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static void routComputerTiming() throws IOException{
        // Initialize objects
        Graph graph = Graph.loadFrom(Path.of("osm-data"));
        List<Integer> nodes = new ArrayList<>();
        CostFunction cf = new CityBikeCF(graph);
        RouteComputer rc = new RouteComputer(graph, cf);
        List<Long> times = new ArrayList<>();

        // Sample nodes uniformly on the map
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                PointCh point = new PointCh(SwissBounds.MIN_E + i * (SwissBounds.MAX_E - SwissBounds.MIN_E) / 100,
                                            SwissBounds.MIN_N + j * (SwissBounds.MAX_N - SwissBounds.MIN_N) / 100);
                int node = graph.nodeClosestTo(point, 100);
                if (node != -1) {
                    nodes.add(node);
                }
            }
        }

        // Sample randomly a pair of nodes in the sampled nodes
        RandomGenerator rng = RandomGeneratorFactory.getDefault().create(RANDOM_SEED);
        for (int i = 0; i < RANDOM_ITERATIONS; i++) {
            int node1 = nodes.get(rng.nextInt(nodes.size()));
            int node2 = nodes.get(rng.nextInt(nodes.size()));

            if (node1 == node2) {
                continue;
            }
            // Benchmark one route computing
            long startTime = System.nanoTime();

            rc.bestRouteBetween(node1, node2);
            long endTime = System.nanoTime();

            long duration = (endTime - startTime);
            times.add(duration);
        }
        
        double average = times.stream()
                              .mapToLong(Long::longValue)
                              .average()
                              .orElse(0.0);
        System.out.println("Average time in seconds: " + average * 1e-9);
    } 
}
