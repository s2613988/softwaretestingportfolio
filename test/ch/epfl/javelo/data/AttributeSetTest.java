package ch.epfl.javelo.data;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;

public class AttributeSetTest {
    // Random parameters
    public final static int RANDOM_SEED = 1957;
    public final static int RANDOM_ITERATIONS = 1000;
    
    // Test of a set of attributes that is empty
    @Test
    public void AttributeSetEmptySet() {
        assertEquals(0L, AttributeSet.of().bits());
    }

    // Test of a set of attributes that contains all attributes
    @Test
    public void AttributeSetAllAttributes() {
        assertEquals((1L << 62) - 1, AttributeSet.of(Attribute.values()).bits());
    }

    // Test of a set of attributes that contains only one attribute
    @Test
    public void AttributeSetOneAttribute() {
        assertEquals(1L << 0, AttributeSet.of(Attribute.values()[0]).bits());
    }

    // Test of a random set of attributes's contains methods
    @Test
    public void AttributeSetRandomSet() {
        RandomGenerator rng = RandomGeneratorFactory.getDefault().create(RANDOM_SEED);

        for (int i = 0; i < RANDOM_ITERATIONS; i++) {
            // Randomize the attributes
            List<Attribute> allAttributesList = Arrays.asList(Attribute.values());
            Collections.shuffle(allAttributesList, new Random(rng.nextLong()));
            Attribute[] allAttributes = allAttributesList.toArray(new Attribute[0]);

            int count = rng.nextInt(62);
            Attribute[] attributes = new Attribute[count];
            for (int j = 0; j < count; j++) {
                attributes[j] = allAttributes[j];
            }

            AttributeSet attributeSet = AttributeSet.of(attributes);
            assertEquals(count, Long.bitCount(attributeSet.bits()));

            // Contains
            for (int j = 0; j < count; j++) {
                assertEquals(true, attributeSet.contains(allAttributes[j]));
            }
            for (int j = count; j < 62; j++) {
                assertEquals(false, attributeSet.contains(allAttributes[j]));
            }
        }
    }

    // Test of the intersection of two random complemantary sets of attributes
    @Test
    public void AttributeSetRandomComplementarySet() {
        RandomGenerator rng = RandomGeneratorFactory.getDefault().create(RANDOM_SEED);
        
        for (int i = 0; i < RANDOM_ITERATIONS; i++) {
            Long bits = rng.nextLong();
            AttributeSet set = new AttributeSet(bits & ((1L << 62) - 1));
            AttributeSet setComplement = new AttributeSet(~bits & ((1L << 62) - 1));

            // Intersection
            assertEquals(false, set.intersects(setComplement));
            assertEquals(false, setComplement.intersects(set));
            assertEquals(true, set.intersects(set));
            assertEquals(true, setComplement.intersects(setComplement));
        }
    }
    
    // Test of a set of attributes's toString method
    @Test
    public void ToString() {
        AttributeSet set = AttributeSet.of(Attribute.SURFACE_PAVED, Attribute.MOTORROAD_YES);
        assertEquals("{motorroad=yes,surface=paved}", set.toString());
    }
    
}
