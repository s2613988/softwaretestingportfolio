package ch.epfl.javelo.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.ByteBuffer;
import java.util.List;

import org.junit.Test;

import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;

public class GraphSectorsTest {
    // Constants
    private static final int NUMBER_OF_SECTOR_PER_SIDES = 128;
    private static final int SECTORS_COUNT = NUMBER_OF_SECTOR_PER_SIDES * NUMBER_OF_SECTOR_PER_SIDES;
    private static final double SECTOR_WIDTH = SwissBounds.WIDTH / NUMBER_OF_SECTOR_PER_SIDES;
    private static final double SECTOR_HEIGHT = SwissBounds.HEIGHT / NUMBER_OF_SECTOR_PER_SIDES;

    // Test sectors in Area works for all sectors in Switzerland
    @Test
    public void SectorsInAreaWorksForAll() {

        ByteBuffer sectorsBuffer = ByteBuffer.allocate(SECTORS_COUNT * (Integer.BYTES + Short.BYTES));
        for (int i = 0; i < SECTORS_COUNT; i += 1) {
            sectorsBuffer.putInt(i);
            sectorsBuffer.putShort((short) 1);
        }

        GraphSectors graphSectors = new GraphSectors(sectorsBuffer);
        for (int i = 0; i < SECTORS_COUNT; i += 1) {

            // Compute the coordinates
            int x = i % NUMBER_OF_SECTOR_PER_SIDES;
            int y = i / NUMBER_OF_SECTOR_PER_SIDES;

            // Compute the absolute coordinates
            double e = SwissBounds.MIN_E + (x + 0.5) * SECTOR_WIDTH;
            double n = SwissBounds.MIN_N + (y + 0.5) * SECTOR_HEIGHT;

            // Compute the sectors in the area should give only one sector
            var sectors = graphSectors.sectorsInArea(new PointCh(e, n), 0.49 * SECTOR_HEIGHT);
            assertEquals(List.of(new GraphSectors.Sector(i, i + 1)), sectors);
        }
    }
    
}
