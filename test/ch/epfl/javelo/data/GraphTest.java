package ch.epfl.javelo.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;

import ch.epfl.javelo.projection.PointCh;

public class GraphTest {
    // Test loadFrom works on Lausanne, Switzerland data
    @Test
    void graphLoadFromWorksOnLausanneData() throws IOException {
        Graph graph = Graph.loadFrom(Path.of("osm-data"));

        // verfiy the graph is loaded correctly
        assertEquals(212679, graph.nodeCount());
        assertEquals(4095, graph.nodeOutEdgeId(2022, 0));
        assertEquals(17.875, graph.edgeLength(2022));
        assertEquals(625.5625, graph.edgeProfile(2022).applyAsDouble(0));
        assertEquals(16, graph.edgeAttributes(2022).bits());
    }

    // Test Node closest to works on Lausanne, Switzerland data
    @Test
    void graphNodeClosestToWorksOnLausanneData() throws IOException {
        var graph = Graph.loadFrom(Path.of("osm-data"));

        assertEquals(159049, graph.nodeClosestTo(new PointCh(2_532_734.8, 1_152_348.0), 100));
        assertEquals(117402, graph.nodeClosestTo(new PointCh(2_538_619.9, 1_154_088.0), 100));
        assertEquals(-1, graph.nodeClosestTo(new PointCh(2_600_000, 1_200_000), 100));
    }
}
