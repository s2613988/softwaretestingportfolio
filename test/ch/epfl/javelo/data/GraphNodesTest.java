package ch.epfl.javelo.data;

import static org.junit.Assert.assertEquals;

import java.nio.IntBuffer;

import org.junit.Test;

public class GraphNodesTest {
    // Sample graph nodes to test on
    public GraphNodes sampleGraphNodes() {
        IntBuffer buffer = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_000_1234
        });
        return new GraphNodes(buffer);
    }

    // Test count
    @Test
    public void GraphNodesCount() {
        GraphNodes graphNodes = sampleGraphNodes();
        assertEquals(1, graphNodes.count());
    }

    // Test nodeE
    @Test
    public void GraphNodesNodeE() {
        GraphNodes graphNodes = sampleGraphNodes();
        assertEquals(2_600_000, graphNodes.nodeE(0), 0.0001);
    }

    // Test nodeN
    @Test
    public void GraphNodesNodeN() {
        GraphNodes graphNodes = sampleGraphNodes();
        assertEquals(1_200_000, graphNodes.nodeN(0), 0.0001);
    }

    // Test outDegree
    @Test
    public void GraphNodesOutDegree() {
        GraphNodes graphNodes = sampleGraphNodes();
        assertEquals(2, graphNodes.outDegree(0));
    }

    // Test edgeId
    @Test
    public void GraphNodesEdgeId() {
        GraphNodes graphNodes = sampleGraphNodes();
        assertEquals(0x1234, graphNodes.edgeId(0, 0));
        assertEquals(0x1235, graphNodes.edgeId(0, 1));
    }
}
