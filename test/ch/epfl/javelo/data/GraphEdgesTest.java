package ch.epfl.javelo.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.junit.Test;

public class GraphEdgesTest {

    // Sample graph edges to test on
    public GraphEdges sampleGraphEdges() {
        ByteBuffer edgesBuffer = ByteBuffer.allocate(10);
        // Inverted, targetNode 12
        edgesBuffer.putInt(0, ~12);
        // Length : 0x10.b m (= 16.6875 m)
        edgesBuffer.putShort(4, (short) 0x10_b);
        // Elevation : 0x10.0 m (= 16.0 m)
        edgesBuffer.putShort(6, (short) 0x10_0);
       
        edgesBuffer.putShort(8, (short) 1);

        IntBuffer profileIds = IntBuffer.wrap(new int[]{
                // Type : 3. first sample index : 1.
                (3 << 30) | 1
        });

        // Sample elevation :
        // 384.0625 m, 384.125 m, 384.25 m, 384.3125 m, 384.375 m, 384.4375 m, 384.5 m, 384.5625 m, 384.6875 m, 384.75 m
        ShortBuffer elevations = ShortBuffer.wrap(new short[]{
                (short) 0,
                (short) 0x180C, (short) 0xFEFF, (short) 0xFFFE, (short) 0xF000
        });

        return new GraphEdges(edgesBuffer, profileIds, elevations);
    }

    // Test isinverted
    @Test
    public void GraphEdgesIsInverted() {
        GraphEdges graphEdges = sampleGraphEdges();
        assertTrue(graphEdges.isInverted(0));
    }

    // Test targetnode
    @Test
    public void GraphEdgesTargetNode() {
        GraphEdges graphEdges = sampleGraphEdges();
        assertEquals(12, graphEdges.targetNodeId(0));
    }

    // Test length
    @Test
    public void GraphEdgesLength() {
        GraphEdges graphEdges = sampleGraphEdges();
        assertEquals(16.6875, graphEdges.length(0), 0.0001);
    }

    // Test elevationgain
    @Test
    public void GraphEdgesElevationGain() {
        GraphEdges graphEdges = sampleGraphEdges();
        assertEquals(16.0, graphEdges.elevationGain(0), 0.0001);
    }

    // Tesr hasProfile
    @Test
    public void GraphEdgesHasProfile() {
        GraphEdges graphEdges = sampleGraphEdges();
        assertTrue(graphEdges.hasProfile(0));
    }

    // Test attributesIndex
    @Test
    public void GraphEdgesAttributesIndex() {
        GraphEdges graphEdges = sampleGraphEdges();
        assertEquals(1, graphEdges.attributesIndex(0));
    }

    // Test profileSamples
    @Test
    public void GraphEdgesProfileSamples() {
        GraphEdges graphEdges = sampleGraphEdges();
        float[] expectedSamples = new float[]{
                384.0625f, 384.125f, 384.25f, 384.3125f, 384.375f,
                384.4375f, 384.5f, 384.5625f, 384.6875f, 384.75f
        };
        assertArrayEquals(expectedSamples, graphEdges.profileSamples(0));
    }
}
